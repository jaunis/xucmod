package integration

import scala.concurrent.duration.DurationInt

import com.fasterxml.jackson.databind.node.ObjectNode

import akka.actor.Actor
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.util.Timeout
import models.XucUser
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.test.FakeApplication
import play.api.test.Helpers.running
import play.libs.Json
import services.CtiRouterFactory
import services.Start
import services.XucEventBus
import services.BrowserMessage

object UseRouters extends App {
  implicit val timeout = Timeout(1 second)

  class EventReceiver(topic: String) extends Actor {
    def receive = {
      case msg => println(s"$topic : message received $msg")
    }
  }

  val appliConfig: Map[String, String] = {
    Map(
      "logger.root" -> "OFF",
      "logger.application" -> "DEBUG",
      "logger.services" -> "DEBUG",
      "xivocti.host" -> "192.168.56.3")
  }

  running(FakeApplication(additionalConfiguration = appliConfig)) {
//    val user1 = XucUser("bruce", "bruce", Some(1000))
//    val user2 = XucUser("irene", "irene", Some(1118))
//    val eventBus = new XucEventBus
//    val allEventReceiver = Akka.system.actorOf(Props(new EventReceiver("/events")), "allEventReceiver")
//    val agentsEventReceiver = Akka.system.actorOf(Props(new EventReceiver("/events/agents/all")), "agentsEventReceiver")
//    val oneAgentEventReceiver = Akka.system.actorOf(Props(new EventReceiver("/events/agents/all/19")), "oneAgentEventReceiver")
//
//    val ctiRouterFactory = CtiRouterFactory.factory
//    val ctiRouter1 = ctiRouterFactory.getRouter(user1)
//    val ctiRouter2 = ctiRouterFactory.getRouter(user2)
//
//    eventBus.subscribe(allEventReceiver, "/events")
//    eventBus.subscribe(allEventReceiver, "/events")
//    eventBus.subscribe(allEventReceiver, "/events")
//    eventBus.subscribe(agentsEventReceiver, "/events/agents/all")
//    eventBus.subscribe(oneAgentEventReceiver, "/events/agents/all/19")
//    ctiRouter1 ! Start(user1)
//    ctiRouter2 ! Start(user2)
//    Thread.sleep(2000)
//
//    ctiRouter1 !  BrowserMessage(createMessage("subscribeToAgentEvents"))
//    Thread.sleep(1350000)
    Akka.system.shutdown()
  }

  private def createMessage(command: String): ObjectNode = {
    val message = Json.newObject()
    message.put("claz", "web")
    message.put("command", command)
  }
}