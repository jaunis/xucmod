package xivo.events

import org.xivo.cti.message.AgentStatusUpdate
import org.xivo.cti.model.AgentStatus
import org.xivo.cti.model.Availability
import org.xivo.cti.model.StatusReason
import org.joda.time.DateTime
import xivo.xucami.models.MonitorState
import xuctest.BaseTest
import scala.collection.mutable.ArrayBuffer
import collection.JavaConversions._
import org.scalactic.Explicitly._

class AgentStatesSpec extends BaseTest {
  import AgentState._
  import AgentState.CallType._
  import AgentState.CallDirection._

  val agentId = 5
  val now = new DateTime
  val queues: java.util.List[Integer] = ArrayBuffer(Integer.valueOf(1), Integer.valueOf(7))

  "AgentState" should {
    "return AgentReady when receiving AVAILABLE" in {
      val status = new AgentStatus("2000", Availability.AVAILABLE, StatusReason.NONE)
      status.setQueues(queues)
      status.setSince(now.toDate)
      val statusUpdate = new AgentStatusUpdate(agentId, status)

      AgentState.fromAgentStatusUpdate(statusUpdate, now) should be(AgentReady(agentId, now, "2000", List(1, 7)))
    }

    "return AgentOnCall calltype external acd incoming  when receiving one of ON_CALL_NONACD_INCOMING_EXTERNAL" in {
      val status = new AgentStatus("2000", Availability.ON_CALL_NONACD_INCOMING_EXTERNAL, StatusReason.NONE)
      status.setSince(now.toDate)

      status.setQueues(queues)
      val statusUpdate = new AgentStatusUpdate(agentId, status)
      AgentState.fromAgentStatusUpdate(statusUpdate, now) should be(AgentOnCall(agentId, now, false, Incoming, External, "2000", List(1, 7), onPause = false))
    }

    "return AgentOnCall calltype internal acd incoming  when receiving one of ON_CALL_NONACD_INCOMING_INTERNAL" in {
      val status = new AgentStatus("2000", Availability.ON_CALL_NONACD_INCOMING_INTERNAL, StatusReason.NONE)
      status.setSince(now.toDate)
      val statusUpdate = new AgentStatusUpdate(agentId, status)

      AgentState.fromAgentStatusUpdate(statusUpdate, now) should be(AgentOnCall(agentId, now, false, Incoming, Internal, "2000", List(), onPause = false))
    }

    "return proper AgentOnCall  when receiving one of ON_CALL_NONACD_OUTGOING_EXTERNAL" in {
      val status = new AgentStatus("2000", Availability.ON_CALL_NONACD_OUTGOING_EXTERNAL, StatusReason.NONE)
      status.setSince(now.toDate)
      val statusUpdate = new AgentStatusUpdate(agentId, status)

      AgentState.fromAgentStatusUpdate(statusUpdate, now) should be(AgentOnCall(agentId, now, false, Outgoing, External, "2000", List(), onPause = false))
    }

    "return proper AgentOnCall when receiving one of ON_CALL_NONACD_OUTGOING_INTERNAL" in {
      val status = new AgentStatus("2000", Availability.ON_CALL_NONACD_OUTGOING_INTERNAL, StatusReason.NONE)
      status.setSince(now.toDate)
      val statusUpdate = new AgentStatusUpdate(agentId, status)

      AgentState.fromAgentStatusUpdate(statusUpdate, now) should be(AgentOnCall(agentId, now, false, Outgoing, Internal, "2000", List(), onPause = false))

    }

    "return AgentOnWrapup when receiving UNAVAILABLE with ON_WRAPUP NOT using event time stamp but local time stamp" in {
      val status = new AgentStatus("2000", Availability.UNAVAILABLE, StatusReason.ON_WRAPUP)
      status.setSince(now.minusSeconds(300).toDate)
      status.setQueues(queues)

      val statusUpdate = new AgentStatusUpdate(agentId, status)

      val agentWrapup = AgentState.fromAgentStatusUpdate(statusUpdate)
      agentWrapup.getSince.toInt should be < 10

      AgentState.fromAgentStatusUpdate(statusUpdate, now) should be(AgentOnWrapup(agentId, now, "2000", List(1, 7)))
    }

    "return proper AgentOnCall when receiving UNAVAILABLE with ON_CALL_ACD" in {
      val status = new AgentStatus("2000", Availability.UNAVAILABLE, StatusReason.ON_CALL_ACD)
      status.setSince(now.toDate)
      val statusUpdate = new AgentStatusUpdate(agentId, status)

      AgentState.fromAgentStatusUpdate(statusUpdate, now) should be(AgentOnCall(agentId, now, true, DirectionUnknown, CallTypeUnknown, "2000", List(), onPause = true))
    }

    "return proper AgentOnCall when receiving ON_CALL_NONACD with another reason" in {
      val status = new AgentStatus("2000", Availability.UNAVAILABLE, StatusReason.ON_CALL_NONACD)
      status.setSince(now.toDate)
      val statusUpdate = new AgentStatusUpdate(agentId, status)

      AgentState.fromAgentStatusUpdate(statusUpdate, now) should be(AgentOnCall(agentId, now, false, DirectionUnknown, CallTypeUnknown, "2000", List(), onPause = true))
    }

    "return AgentOnPause when receiving UNAVAILABLE with another reason dated from now" in {

      val status = new AgentStatus("2000", Availability.UNAVAILABLE, StatusReason.NONE)
      status.setSince(new DateTime(250000).toDate)
      status.setQueues(queues)

      val statusUpdate = new AgentStatusUpdate(agentId, status)

      val agentPaused  =AgentState.fromAgentStatusUpdate(statusUpdate)
      agentPaused.getSince.toInt should be <(10)

      AgentState.fromAgentStatusUpdate(statusUpdate) should be(AgentOnPause(agentId, now, "2000", List(1, 7)))
    }

    "return AgentLoggedOut when receiving LOGGED_OUT" in {
      val status = new AgentStatus("2000", Availability.LOGGED_OUT, StatusReason.NONE)
      status.setSince(now.toDate)
      status.setQueues(queues)
      val statusUpdate = new AgentStatusUpdate(agentId, status)

      AgentState.fromAgentStatusUpdate(statusUpdate, now) should be(AgentLoggedOut(agentId, now, "2000", List(1, 7)))
    }

    "return a complete json transform for agent pause" in {
      val s: String = AgentState.toJson(AgentOnPause(agentId, new DateTime().minusSeconds(57), "2000",List(2,4,9), Some("dejeuner"))).toString.replace(" ","")

      s should  be("""{"name":"AgentOnPause","agentId":5,"phoneNb":"2000","since":57,"queues":[2,4,9],"cause":"dejeuner"}""".replace(" ",""))
    }

    "return a complete json transform for agent on call" in {
      val s: String = AgentState.toJson(AgentOnCall( agentId, new DateTime().minusSeconds(52), false, DirectionUnknown,
        CallTypeUnknown, "2000",List(2,4,9), onPause=true, monitorState = MonitorState.PAUSED)).toString.replace(" ","")

      s should be(("""{"name":"AgentOnCall","agentId":5,"phoneNb":"2000","since":52,"queues":[2,4,9],"cause":"","acd":false,"direction":"DirectionUnknown","callType":"CallTypeUnknown","monitorState":"PAUSED"}""").replace(" ",""))
    }

    "AgentOnPause with a new cause reset start time" in {
      val now = new DateTime()
      val pauseState = AgentOnPause(agentId, new DateTime().minusSeconds(57), "2000",List(2,4,9), Some("dejeuner"))

      pauseState.withCause("coffee").changed.getMillis should be(now.getMillis +- 2000)

    }
  }
}