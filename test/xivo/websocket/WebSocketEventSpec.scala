package xivo.websocket

import java.util.Date

import com.fasterxml.jackson.databind.JsonNode
import models.{QueueCallList, RichDirectoryResult, XucUser}
import org.joda.time.DateTime
import org.scalatest.{Matchers, WordSpec}
import org.xivo.cti.message._
import org.xivo.cti.model._
import play.libs.Json
import services.XucStatsEventBus.{AggregatedStatEvent, Stat}
import services.agent.{AgentStatistic, StatDateTime, StatPeriod, Statistic}
import services.config.AgentDirectoryEntry
import services.config.ConfigDispatcher.AgentDirectory
import services.line.LineState
import xivo.ami.AmiBusConnector.{AgentListenStarted, AgentListenStopped, CallData, LineEvent}
import xivo.events.AgentState.AgentReady
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.{Agent, AgentGroup, AgentQueueMember}
import xivo.websocket.WSMsgType.WSMsgType
import xivo.xucami.models.QueueCall

import scala.collection.JavaConversions._

class WebSocketEventSpec extends WordSpec with Matchers
{
  "Websocket event" should {

    "return an evtSheet message when asked for" in {
      val sheet = new Sheet()
      sheet.channel = "testChannel"
      sheet.compressed = true
      sheet.serial = "12333"
      sheet.timenow = new Date()

      val evtSheet = WebsocketEvent.createEvent(sheet)
      evtSheet.get("msgType").asText() should be("Sheet")

      messageTypeShouldBe(evtSheet, WSMsgType.Sheet)
      ctiMessage(evtSheet) should be(Json.toJson(sheet).toString)
    }

    "return a json agent message" in {
      val agent = Agent(32,"Noe", "Rasp", "3457","default", 45)

      val event = WebsocketEvent.createEvent(agent)

      messageTypeShouldBe(event, WSMsgType.AgentConfig)
      ctiMessage(event) should be("""{"id":32,"firstName":"Noe","lastName":"Rasp","number":"3457","context":"default","groupId":45}""")
    }

    "return a json agent directory message" in {
      val agent = Agent(32,"Noe", "Rasp", "3457","default", 45)
      val agentState = AgentReady(3, new DateTime(), "1001", List())
      val agDir = AgentDirectory(List(AgentDirectoryEntry(agent, agentState)))

      val event = WebsocketEvent.createEvent(agDir)

      messageTypeShouldBe(event, WSMsgType.AgentDirectory)
      ctiMessage(event) should be("{\"directory\":[{\"agent\":{\"id\":32,\"firstName\":\"Noe\",\"lastName\":\"Rasp\",\"number\":\"3457\",\"context\":\"default\",\"groupId\":45},\"agentState\":{\"name\":\"AgentReady\",\"agentId\":3,\"phoneNb\":\"1001\",\"since\":0,\"queues\":[],\"cause\":\"\"}}]}")
    }

    "return a json queue member message" in {
      val qm = AgentQueueMember(23,45,2)

      val event = WebsocketEvent.createEvent(qm)

      messageTypeShouldBe(event, WSMsgType.QueueMember)
      ctiMessage(event) should be ("{\"agentId\":23,\"queueId\":45,\"penalty\":2}")

    }
    "return an agent state event" in {
      val agentState = AgentReady(3, new DateTime(), "1001", List(2,3))

      val event = WebsocketEvent.createEvent(agentState)

      messageTypeShouldBe(event, WSMsgType.AgentStateEvent)
      ctiMessage(event) should be ("{\"name\":\"AgentReady\",\"agentId\":3,\"phoneNb\":\"1001\",\"since\":0,\"queues\":[2,3],\"cause\":\"\"}")

    }

    "return a link state event" in {
      val ls = LinkStatusUpdate(LinkState.down)

      val event = WebsocketEvent.createEvent(ls)

      messageTypeShouldBe(event, WSMsgType.LinkStatusUpdate)
      ctiMessage(event) should be ("{\"status\":\"down\"}")

    }

    "return a directory result event" in {
      val rds = new RichDirectoryResult(List("name"))

      val event = WebsocketEvent.createEvent(rds)

      messageTypeShouldBe(event, WSMsgType.DirectoryResult)

      ctiMessage(event) should be("{\"headers\":[\"name\"],\"entries\":[]}")
    }
    "return a list of agents event" in {
      val agent = Agent(32,"Noe", "Rasp", "3457","default", 45)

      val event = WebsocketEvent.createAgentListEvent(List(agent))

      messageTypeShouldBe(event, WSMsgType.AgentList)

      ctiMessage(event) should be("[{\"id\":32,\"firstName\":\"Noe\",\"lastName\":\"Rasp\",\"number\":\"3457\",\"context\":\"default\",\"groupId\":45}]")
    }
    "return a list of agent groups" in {
      val agentGroup = AgentGroup(Some(3),"boats", Some("Boats"))

      val event = WebsocketEvent.createAgentGroupsEvent(List(agentGroup))

      messageTypeShouldBe(event, WSMsgType.AgentGroupList)

      ctiMessage(event) should be("[{\"id\":3,\"name\":\"boats\",\"displayName\":\"Boats\"}]")
    }
    "return a list of agent queue members "  in {
      val qm = AgentQueueMember(23,45,2)

      val event = WebsocketEvent.createAgentQueueMembersEvent(List(qm))

      messageTypeShouldBe(event, WSMsgType.QueueMemberList)

      ctiMessage(event) should be("[{\"agentId\":23,\"queueId\":45,\"penalty\":2}]")
    }

    "return a statistic event" in {
      import xivo.models.XivoObject.ObjectType._

      val stat = AggregatedStatEvent(ObjectDefinition(Queue,Some(32)), List(Stat("totalnumber", 32)))

      val event = WebsocketEvent.createEvent(stat)

      messageTypeShouldBe(event, WSMsgType.QueueStatistics)
      ctiMessage(event) should be("{\"queueId\":32,\"counters\":[{\"statName\":\"totalnumber\",\"value\":32.0}]}")
    }

    "return a logged on event" in {
      val event = WebsocketEvent.createLoggedOnEvent()

      messageTypeShouldBe(event, WSMsgType.LoggedOn)
    }

    "return user statuses event" in {
      val avail = new UserStatus("available")
      avail.setLongName("Disponible")
      avail.setColor("#FF032D")
      avail.addAction(new Action("queueunpause_all","1"))
      val userStatuses = java.util.Arrays.asList(avail, new UserStatus("disconnected"))
      val user = XucUser("bob", "pwd")
      val userId = "5"

      val event = WebsocketEvent.createEvent(userStatuses)

      messageTypeShouldBe(event, WSMsgType.UsersStatuses)

      ctiMessage(event) should be("[{\"name\":\"available\",\"color\":\"#FF032D\",\"longName\":\"Disponible\",\"actions\":[{\"name\":\"queueunpause_all\",\"parameters\":\"1\"}]},{\"name\":\"disconnected\",\"color\":null,\"longName\":null,\"actions\":[]}]")
    }

    "return queue config event" in {
      val queue = new QueueConfigUpdate()
      queue.setName("q1")
      queue.setDisplayName("Queue One")
      queue.setNumber("3000")
      queue.setId(32)

      val event = WebsocketEvent.createEvent(queue)

      messageTypeShouldBe(event, WSMsgType.QueueConfig)

      ctiMessage(event) should be ("{\"id\":32,\"context\":\"\",\"name\":\"q1\",\"displayName\":\"Queue One\",\"number\":\"3000\"}")
    }
    "return a list of queue config" in {
      val queue = new QueueConfigUpdate()
      queue.setName("q1")
      queue.setDisplayName("Queue One")
      queue.setNumber("3010")
      queue.setId(32)


      val event = WebsocketEvent.createQueuesEvent(List(queue))

      messageTypeShouldBe(event, WSMsgType.QueueList)

      ctiMessage(event) should be ("[{\"id\":32,\"context\":\"\",\"name\":\"q1\",\"displayName\":\"Queue One\",\"number\":\"3010\"}]")
    }

    "return queue statistic event" in {

      val qstat = new QueueStatistics()
      qstat.setQueueId(45)
      qstat.addCounter(new Counter(StatName.WaitingCalls,34))

      val event = WebsocketEvent.createEvent(qstat)

      messageTypeShouldBe(event, WSMsgType.QueueStatistics)

      ctiMessage(event) should be ("{\"queueId\":45,\"counters\":[{\"statName\":\"WaitingCalls\",\"value\":34}]}")

    }
    "return phone hint status" in {
      val ph: PhoneHintStatus = PhoneHintStatus.getHintStatus(0)

      val event = WebsocketEvent.createEvent(ph)

      messageTypeShouldBe(event, WSMsgType.PhoneStatusUpdate)

      ctiMessage(event) should be ("{\"status\":\"AVAILABLE\"}")
    }

    "return an error" in {

      val event = WebsocketEvent.createError(WSMsgType.Error,"Unable to dial this number",Map("Extenstion"->"3215"))

      messageTypeShouldBe(event, WSMsgType.Error)

      ctiMessage(event) should be ("{\"Error\":\"Unable to dial this number\",\"Extenstion\":\"3215\"}")

    }

    "return a user status update" in {
      val userStatusUpdate = new UserStatusUpdate()
      userStatusUpdate.setUserId(32)
      userStatusUpdate.setStatus("donotdisturb")

      val event = WebsocketEvent.createEvent(userStatusUpdate)

      messageTypeShouldBe(event, WSMsgType.UserStatusUpdate)

      ctiMessage(event) should be ("{\"status\":\"donotdisturb\"}")
    }

    "return an agent error on agent request error invalid extension" in {
      val agentRequestError = new IpbxCommandResponse("agent_login_invalid_exten", new Date)

      val event = WebsocketEvent.createEvent(agentRequestError)

      messageTypeShouldBe(event, WSMsgType.AgentError)
      ctiMessage(event) should be ("{\"Error\":\"PhoneNumberUnknown\"}")

    }
    "return an agent error on agent request error agent_login_exten_in_use" in {
      val agentRequestError = new IpbxCommandResponse("agent_login_exten_in_use", new Date)

      val event = WebsocketEvent.createEvent(agentRequestError)

      messageTypeShouldBe(event, WSMsgType.AgentError)
      ctiMessage(event) should be ("{\"Error\":\"PhoneNumberAlreadyInUse\"}")

    }
    "return an agent error on agent request error unkown message" in {
      val agentRequestError = new IpbxCommandResponse("agent_error", new Date)

      val event = WebsocketEvent.createEvent(agentRequestError)

      messageTypeShouldBe(event, WSMsgType.AgentError)
      ctiMessage(event) should be ("{\"Error\":\"Unknown:agent_error\"}")

    }

    "return an line event" in {
      val lineEvent = LineEvent(3300, CallData("callId", LineState.BUSY, "3300"))

      val event = WebsocketEvent.createEvent(lineEvent)

      messageTypeShouldBe(event, WSMsgType.LineStateEvent)

      ctiMessage(event) should be ("{\"lineNumber\":3300,\"callData\":{\"callId\":\"callId\",\"lineState\":\"BUSY\"}}")

    }

    "return a conference list" in {
      val startTime = new Date(24560321)
      val meetme = new Meetme("4000", "test", true, startTime, List(new MeetmeMember(1, startTime, false, "John Doe", "2000")))

      val event = WebsocketEvent.createConferenceListEvent(List(meetme))

      messageTypeShouldBe(event, WSMsgType.ConferenceList)
      ctiMessage(event) should be ("[{\"number\":\"4000\",\"name\":\"test\",\"pinRequired\":true,\"startTime\":24560321,\"members\":[" +
        "{\"joinOrder\":1,\"joinTime\":24560321,\"muted\":false,\"name\":\"John Doe\",\"number\":\"2000\"}]}]")
    }

    "return a user config update " in {

      val userConfigUpdate = new UserConfigUpdate

      val event = WebsocketEvent.createEvent(userConfigUpdate)

      messageTypeShouldBe(event, WSMsgType.UserConfigUpdate)

      ctiMessage(event) should be ("{\"userId\":0,\"dndEnabled\":false,\"naFwdEnabled\":false,\"naFwdDestination\":null,\"uncFwdEnabled\":false," +
        "\"uncFwdDestination\":null,\"busyFwdEnabled\":false,\"busyFwdDestination\":null,\"firstName\":null,\"lastName\":null,\"fullName\":null," +
        "\"mobileNumber\":null,\"agentId\":0,\"lineIds\":[],\"voiceMailId\":0,\"voiceMailEnabled\":false}")

    }

    "return an agent listen started" in {
      val agls = AgentListenStarted("8741",Some(45))

      val event = WebsocketEvent.createEvent(agls)

      messageTypeShouldBe(event, WSMsgType.AgentListen)

      ctiMessage(event) should be("{\"started\":true,\"phoneNumber\":\"8741\",\"agentId\":45}")

    }

    "return an agent listen stopped" in {
      val aglstopped = AgentListenStopped("4561",Some(27))

      val event = WebsocketEvent.createEvent(aglstopped)

      messageTypeShouldBe(event, WSMsgType.AgentListen)

      ctiMessage(event) should be("{\"started\":false,\"phoneNumber\":\"4561\",\"agentId\":27}")
    }

    "return a period agent statistic" in {
      val agstat  = AgentStatistic(34, List(Statistic("LoginTime",StatPeriod(34))))

      val event = WebsocketEvent.createEvent(agstat)

      messageTypeShouldBe(event, WSMsgType.AgentStatistics)

      ctiMessage(event) should be("{\"id\":34,\"statistics\":[{\"name\":\"LoginTime\",\"value\":34}]}")
    }
    "return a date agent statistic" in {
      val agstat  = AgentStatistic(34, List(Statistic("LoginTime",StatDateTime(new DateTime(2005, 3, 26, 12, 20, 34, 58)))))

      val event = WebsocketEvent.createEvent(agstat)

      messageTypeShouldBe(event, WSMsgType.AgentStatistics)

      ctiMessage(event) should be("{\"id\":34,\"statistics\":[{\"name\":\"LoginTime\",\"value\":\"2005-03-26T12:20:34.058+01:00\"}]}")
    }
    "return QueueCalls" in {
      val queueCalls = QueueCallList(5, List(
        QueueCall(1, "foo", "333456789", new DateTime(2005, 3, 26, 12, 20, 34, 58)),
        QueueCall(2, "bar", "4745468793", new DateTime(2005, 3, 26, 12, 20, 35, 40))
      ))

      val event = WebsocketEvent.createEvent(queueCalls)

      messageTypeShouldBe(event, WSMsgType.QueueCalls)
      ctiMessage(event) should be(
        """{"queueId":5,"calls":[{"position":1,"name":"foo","number":"333456789","queueTime":"2005-03-26T12:20:34.058+01:00"},""" +
        """{"position":2,"name":"bar","number":"4745468793","queueTime":"2005-03-26T12:20:35.040+01:00"}]}""")
    }

  }
  private def messageTypeShouldBe(event : JsonNode, msgType: WSMsgType) = event.get(WebsocketEvent.MsgType).asText() should be (msgType.toString)

  private def ctiMessage(event: JsonNode) = event.get(WebsocketEvent.CtiMessage).toString

}
