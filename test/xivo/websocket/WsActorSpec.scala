package xivo.websocket

import akka.actor.PoisonPill
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.XucUser
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import play.libs.Json
import services.request._
import services.user.UserRight
import services.{BrowserMessage, Leave}
import xivo.websocket.WsActor.WsConnected
import xivo.websocket.WsBus.WsContent

class WsActorSpec extends TestKitSpec("WsActorSpec")  with MockitoSugar {

  class Helper {
    val router = TestProbe()
    val out = TestProbe()
    val user = XucUser("testUser","")
    val wsBus = mock[WsBus]
    val requestDecoder = mock[RequestDecoder]
    val wsEncoder= mock[WsEncoder]
    val right = mock[UserRight]

    def actor() = {
      val a = TestActorRef(new WsActor(user,out.ref, router.ref, right, wsBus, requestDecoder, wsEncoder=wsEncoder))
      (a, a.underlyingActor)

    }
  }

  "WsActor" should {
    "Subscribe to wsbus at startup and send wsconnected message to router on use router message"  in new Helper {
      val (ref,_) = actor()

      verify(wsBus).subscribe(ref,WsBus.browserTopic("testUser"))
      router.expectMsg(WsConnected(ref,user))

    }
    "usubscribe from bus on stop" in new Helper {
      val (ref,_) = actor()

      verify(wsBus).subscribe(ref,WsBus.browserTopic("testUser"))

      ref ! PoisonPill

      verify(wsBus).unsubscribe(ref)
      router.expectMsgAllOf(WsConnected(ref,user), Leave(ref))

    }

    "forward jsvalue messages to request decoder and send result to router" in new Helper {
      val (ref,_) = actor()

      val message = play.api.libs.json.Json.toJson("message")
      val decoded = BrowserMessage(Json.newObject())
      val baseRequest = BaseRequest(ref, decoded)
      stub(requestDecoder.decode(message)).toReturn(decoded)

      ref ! message

      verify(requestDecoder).decode(message)
      router.expectMsgAllOf(WsConnected(ref,user),baseRequest)

    }

    "Send back ping message to sender" in new Helper {
      val (ref,_) = actor()

      val pingMsg = play.api.libs.json.Json.obj("claz"->"ping")

      stub(requestDecoder.decode(pingMsg)).toReturn(Ping)

      ref ! pingMsg

      out.expectMsg(play.libs.Json.parse(pingMsg.toString()))

    }

    "send xuc request to router" in new Helper {
      val (ref,_) = actor()
      val message = play.api.libs.json.Json.toJson("message")

      case object FakeRequest extends  XucRequest

      stub(requestDecoder.decode(message)).toReturn(FakeRequest)

      ref ! message

      router.expectMsgAllOf(WsConnected(ref,user),BaseRequest(ref, FakeRequest))

    }
    "Send error to sender on invalid request " in new Helper {
      val (ref,_) = actor()

      val message = play.api.libs.json.Json.toJson("message")

      val invalidRequest = InvalidRequest(XucRequest.errors.invalid,message.toString())
      stub(requestDecoder.decode(message)).toReturn(invalidRequest)

      ref ! message

      out.expectMsg(WebsocketEvent.createError(WSMsgType.Error,invalidRequest.toString))

    }

    "forward wscontent json message to browser" in new Helper {
      val (ref,_) = actor()

      val jsonContent = Json.newObject()
      val message = WsContent(jsonContent)

      ref ! message

      out.expectMsg(jsonContent)
    }

    "filter and encode all other messages to browser" in new Helper {
      val (ref,_) = actor()
      val message = "dummy"
      val filteredMessage = "dummy2"
      val jsonContent = Json.newObject()
      val wsMessage = WsContent(jsonContent)

      stub(right.filter(message)).toReturn(Some(filteredMessage))
      stub(wsEncoder.encode(filteredMessage)).toReturn(Some(wsMessage))

      ref ! message

      out.expectMsg(jsonContent)
    }
  }

}
