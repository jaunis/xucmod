package services.config

import akka.actor.Props
import akka.testkit.{TestProbe, TestActorRef}
import akkatest.TestKitSpec
import org.mockito.Mockito.{stub, verifyZeroInteractions, verify, times, reset}
import org.scalatest.mock.MockitoSugar
import services.line.{LineState, XucLineFactory}
import xivo.ami.AmiBusConnector.{AgentListenStopped, AgentListenStarted, LineEvent, CallData}

class XucLineManagerSpec extends TestKitSpec("LineManagerSpec")
with MockitoSugar {

  class Helper {
    val lineFactory = mock[XucLineFactory]

    def actor = {
      val a = TestActorRef[XucLineManager](Props(new XucLineManager(lineFactory)))
      (a, a.underlyingActor)
    }
  }

  "Line Manager" should {
    "create new line on CreateLine event received" in new Helper {
      val (ref, lineManager) = actor
      val line = TestProbe()
      val createLine = CreateLine(13)
      stub(lineFactory.create(13, lineManager.context)).toReturn(line.ref)
      ref ! createLine

      verify(lineFactory).create(13, lineManager.context)
      lineManager.lines.get(13) shouldBe Some(line.ref)

    }
    "do not create an already created actor for a line" in new Helper {

      val (ref, lineManager) = actor
      val line = TestProbe()
      val createLine = CreateLine(13)
      stub(lineFactory.create(13, lineManager.context)).toReturn(line.ref)
      ref ! createLine
      ref ! createLine

      verify(lineFactory,times(1)).create(13, lineManager.context)
      lineManager.lines.get(13) shouldBe Some(line.ref)

    }

    "use already created line on event received" in new Helper {
      val (ref, lineManager) = actor
      val line = TestProbe()
      stub(lineFactory.create(14, lineManager.context)).toReturn(line.ref)
      ref ! CreateLine(14)

      val lineEvent1 = LineEvent(14, CallData("call1", LineState.DOWN, "14"))
      val lineEvent2 = LineEvent(14, CallData("call1", LineState.RINGING, "14"))
      ref ! lineEvent1
      ref ! lineEvent2

      verify(lineFactory, times(1)).create(14, lineManager.context)
      line.expectMsgAllOf(lineEvent1, lineEvent2)
    }

    "silenty discard events for unexisting lines" in new Helper {
      val (ref, lineManager) = actor
      val lineEvent1 = LineEvent(14, CallData("call1", LineState.DOWN, "14"))
      val lineEvent2 = LineEvent(14, CallData("call1", LineState.RINGING, "14"))
      ref ! lineEvent1
      ref ! lineEvent2
      verifyZeroInteractions(lineFactory)
    }

    "send agentlistenestarted to listened" in new Helper {
      val (ref, lineManager) = actor
      val listenedLine = TestProbe()
      stub(lineFactory.create(2500, lineManager.context)).toReturn(listenedLine.ref)
      ref ! CreateLine(2500)

      ref ! AgentListenStarted("2500", Some(78))

      listenedLine.expectMsg(AgentListenStarted("2500", Some(78)))

    }

    "send agent listen stopped to listened" in new Helper {
      val (ref, lineManager) = actor
      val listenedLine = TestProbe()
      stub(lineFactory.create(8400, lineManager.context)).toReturn(listenedLine.ref)
      ref ! CreateLine(8400)

      ref ! AgentListenStopped("8400", Some(458))

      listenedLine.expectMsg(AgentListenStopped("8400", Some(458)))
    }
  }
}
