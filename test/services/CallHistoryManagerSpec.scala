package services

import akka.actor.{ActorRef, Props}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.mockito.Mockito.stub
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mock.MockitoSugar
import play.api.libs.json.{JsArray, Json}
import services.config.ConfigRepository
import services.request.{AgentCallHistoryRequest, BaseRequest, UserCallHistoryRequest}
import xivo.models.{CallDetail, CallHistory, CallStatus}
import xivo.network._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class CallHistoryManagerSpec extends TestKitSpec("CtiRouterSpec") with MockitoSugar  with BeforeAndAfterEach {

  val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  var requester: TestProbe = null
  var requesterRef: ActorRef = null
  var recordingWs: RecordingWS = null
  var historyMgr: ActorRef = null

  override def beforeEach(): Unit = {
    requester = TestProbe()
    requesterRef = requester.ref
    recordingWs = mock[RecordingWS]
    historyMgr = TestActorRef[CallHistoryManager](Props(new CallHistoryManager(recordingWs)))
    ConfigRepository.repo = mock[ConfigRepository]
  }


  "A CallHistoryManager" should {
    "send agent call history to the requester" in {
      val rq = BaseRequest(requesterRef, AgentCallHistoryRequest(10, "toto"))
      stub(ConfigRepository.repo.agentNumberFromUsername("toto")).toReturn(Some("1002"))
      val json = JsArray(List(
        Json.obj(
        "start" -> "2014-01-01 08:00:00",
        "duration" -> "00:14:23",
        "agent" -> "1002",
        "queue" -> "test_queue",
        "src_num" -> "3354789",
        "dst_num" -> "44485864")))
      stub(recordingWs.getCallHistory(10, "1002")).toReturn(Future(HistoryServerResponse(json)))

      historyMgr ! rq

      requester.expectMsg(CallHistory(List(
        CallDetail(format.parseDateTime("2014-01-01 08:00:00"), Some(new Period(0, 14, 23, 0)), "3354789", "44485864", CallStatus.Answered))))
    }

    "login and send agent call history to the requester" in {
      val rq = BaseRequest(requesterRef, AgentCallHistoryRequest(10, "toto"))
      stub(ConfigRepository.repo.agentNumberFromUsername("toto")).toReturn(Some("1002"))
      val json = JsArray(List(
        Json.obj(
        "start" -> "2014-01-01 08:00:00",
        "duration" -> "00:14:23",
        "agent" -> "1002",
        "queue" -> "test_queue",
        "src_num" -> "3354789",
        "dst_num" -> "44485864")))
      stub(recordingWs.getCallHistory(10, "1002")).toReturn(Future(throw new ForbiddenException)).toReturn(Future(HistoryServerResponse(json)))
      stub(recordingWs.login()).toReturn(Future(HistoryServerResponse(Json.obj())))

      historyMgr ! rq

      requester.expectMsg(CallHistory(List(
        CallDetail(format.parseDateTime("2014-01-01 08:00:00"), Some(new Period(0, 14, 23, 0)), "3354789", "44485864", CallStatus.Answered))))
    }

    "send user call history to the requester" in {
      val rq = BaseRequest(requesterRef, UserCallHistoryRequest(10, "toto"))
      val interface = "SIP/agbef"
      stub(ConfigRepository.repo.interfaceFromUsername("toto")).toReturn(Some(interface))
      val json = JsArray(List(
        Json.obj(
        "start" -> "2014-01-01 08:00:00",
        "duration" -> "00:14:23",
        "status" -> "emitted",
        "src_num" -> "3354789",
        "dst_num" -> "44485864")))
      stub(recordingWs.getUserCallHistory(10, interface)).toReturn(Future(HistoryServerResponse(json)))

      historyMgr ! rq

      requester.expectMsg(CallHistory(List(
        CallDetail(format.parseDateTime("2014-01-01 08:00:00"), Some(new Period(0, 14, 23, 0)), "3354789", "44485864", CallStatus.Emitted))))
    }

    "login and send user call history to the requester" in {
      val rq = BaseRequest(requesterRef, UserCallHistoryRequest(10, "toto"))
      val interface = "SIP/agbef"
      stub(ConfigRepository.repo.interfaceFromUsername("toto")).toReturn(Some(interface))
      val json = JsArray(List(
        Json.obj(
        "start" -> "2014-01-01 08:00:00",
        "duration" -> "00:14:23",
        "status" -> "missed",
        "src_num" -> "3354789",
        "dst_num" -> "44485864")))
      stub(recordingWs.getUserCallHistory(10, interface)).toReturn(Future(throw new ForbiddenException)).toReturn(Future(HistoryServerResponse(json)))
      stub(recordingWs.login()).toReturn(Future(HistoryServerResponse(Json.obj())))

      historyMgr ! rq

      requester.expectMsg(CallHistory(List(
        CallDetail(format.parseDateTime("2014-01-01 08:00:00"), Some(new Period(0, 14, 23, 0)), "3354789", "44485864", CallStatus.Missed))))
    }
  }
}
