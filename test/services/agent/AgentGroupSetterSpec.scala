package services.agent

import akka.actor.Props
import akka.testkit.{TestProbe, TestActorRef}
import akkatest.TestKitSpec
import org.mockito.Mockito.verify
import org.scalatest.mock.MockitoSugar
import services.ActorFactory
import services.request.SetAgentGroup
import xivo.models.{RefreshAgent, AgentFactory}

class AgentGroupSetterSpec extends TestKitSpec("AgentGroupSetter") with MockitoSugar {

  class Helper {
    val configDispatcher = TestProbe()
    val agentGroup = mock[AgentFactory]

    trait TestActorFactory extends ActorFactory {
      override val configDispatcherURI = configDispatcher.ref.path.toString
    }

    def actor = {
      val a = TestActorRef[AgentGroupSetter](Props(new AgentGroupSetter(agentGroup) with TestActorFactory))
      (a, a.underlyingActor)
    }
  }


  "An AgentGroupSetter" should {
    "use the agentGroup object to update agent group association" in new Helper {
      val (agentId, groupId) = (54, 3)
      val (ref, setter) = actor
      val request = SetAgentGroup(agentId, groupId)

      ref ! request

      verify(agentGroup).moveAgentToGroup(agentId, groupId)
      configDispatcher.expectMsg(RefreshAgent(agentId))
    }
  }

}
