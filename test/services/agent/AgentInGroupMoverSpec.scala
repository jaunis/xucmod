package services.agent

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import services.ActorFactory
import services.agent.AgentInGroupAction.AgentsDestination
import services.config.ConfigDispatcher._
import xivo.models.Agent
import scala.concurrent.duration.DurationInt

class AgentInGroupMoverSpec extends TestKitSpec("agentingroupmover") {


  class Helper {
    val configDispatcher = TestProbe()

    trait TestActorFactory extends  ActorFactory {
      override val configDispatcherURI = configDispatcher.ref.path.toString
    }

    def actor(groupId: Long, queueId: Long, penalty : Int) = {
      val a = TestActorRef(Props(new AgentInGroupMover(groupId, queueId, penalty) with TestActorFactory))
      (a,a.underlyingActor)
    }
  }



  "an agent group mover" should {

    "should set agent in new queue and remove from old queue upon reception of agents" in new Helper {

      val (groupId, fromQueueId, fromPenalty) = (7,44,8)
      val (toQueueId, toPenalty) = (22,2)
      val (ref,_) = actor(groupId,fromQueueId,fromPenalty)

      val agents = List(Agent(1,"John","Malt","33784","default",groupId))

      ref ! AgentsDestination(toQueueId, toPenalty)

      ref !  AgentList(agents)

      configDispatcher.expectMsgAllOf(RequestConfig(ref,GetAgents(groupId, fromQueueId, fromPenalty)),
                                        ConfigChangeRequest(ref,SetAgentQueue(1,toQueueId,toPenalty)),
                                        ConfigChangeRequest(ref,RemoveAgentFromQueue(1,fromQueueId)))

    }
    "should not remove agent from queue when agents are move inside the same queue" in new Helper {
      val (groupId, fromQueueId, fromPenalty) = (7,44,8)
      val (toQueueId, toPenalty) = (fromQueueId,2)
      val (ref,_) = actor(groupId,fromQueueId,fromPenalty)

      val agents = List(Agent(1,"John","Malt","33784","default",groupId))

      ref ! AgentsDestination(toQueueId, toPenalty)

      ref !  AgentList(agents)

      configDispatcher.expectMsgAllOf(RequestConfig(ref,GetAgents(groupId, fromQueueId, fromPenalty)),
        ConfigChangeRequest(ref,SetAgentQueue(1,toQueueId,toPenalty)))
      configDispatcher.expectNoMsg(100 millis)

    }

  }
}
