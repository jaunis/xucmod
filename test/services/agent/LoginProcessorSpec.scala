package services.agent

import akka.actor.{ReceiveTimeout, Props}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.joda.time.DateTime
import org.scalatest.mock.MockitoSugar
import services.{ActorFactory, XucEventBus}
import services.request.{AgentLoginRequest, BaseRequest}
import xivo.events.AgentLoginError
import xivo.events.AgentState.AgentLogin
import xivo.xuc.api.{RequestError, RequestTimeout, RequestSuccess}

class LoginProcessorSpec extends TestKitSpec("LoginProcessorSpec") with MockitoSugar {

  val eventBus = mock[XucEventBus]
  val configManager = TestProbe()

  trait TestActorFactory extends ActorFactory {
    override val configManagerURI = (configManager.ref.path).toString
  }

  class Helper {
    def actor = {
      val a = TestActorRef[LoginProcessor](Props(new LoginProcessor(eventBus) with TestActorFactory))
      (a, a.underlyingActor)
    }
  }

  "A LoginProcessor" should {
    "process Login requests" in new Helper {
      val (ref, _) = actor
      ref ! AgentLoginRequest(None, Some("1010"))
      configManager.expectMsg(BaseRequest(ref, AgentLoginRequest(None, Some("1010"))))
    }

    "process AgentLogin event" in new Helper {
      val (ref, processor) = actor
      val requester = TestProbe()
      val request = AgentLoginRequest(None, Some("1010"))
      processor.expectingResult(requester.ref, request)(AgentLogin(11L, new DateTime(), "1010", List(1)))
      requester.expectMsgClass(classOf[RequestSuccess])
    }

    "process AgentLoginError event" in new Helper {
      val (ref, processor) = actor
      val requester = TestProbe()
      val request = AgentLoginRequest(None, Some("1010"))
      processor.expectingResult(requester.ref, request)(AgentLoginError("testError"))
      requester.expectMsgClass(classOf[RequestError])
    }

    "forward RequestResult events" in new Helper {
      val (ref, processor) = actor
      val requester = TestProbe()
      val request = AgentLoginRequest(None, Some("1010"))
      val result = RequestSuccess("testOK")
      processor.expectingResult(requester.ref, request)(result)
      requester.expectMsg(result)
    }

    "process ReceiveTimeout event in receive state" in new Helper {
      val (ref, processor) = actor
      val requester = TestProbe()
      watch(ref)
      ref ! ReceiveTimeout
      expectTerminated(ref)
    }

    "process ReceiveTimeout event in expectingResult state" in new Helper {
      val (ref, processor) = actor
      val requester = TestProbe()
      val request = AgentLoginRequest(None, Some("1010"))
      processor.expectingResult(requester.ref, request)(ReceiveTimeout)
      requester.expectMsgClass(classOf[RequestTimeout])
    }
  }
}

