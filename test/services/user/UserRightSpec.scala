package services.user

import org.joda.time.DateTime
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.xivo.cti.message.QueueConfigUpdate
import play.api.libs.json.Json
import services.XucStatsEventBus.AggregatedStatEvent
import services.agent.AgentStatistic
import services.config.ConfigDispatcher.{AgentList, AgentGroupList, AgentQueueMemberList, QueueList}
import services.config.ConfigRepository
import xivo.events.AgentState.AgentLogin
import xivo.models.{Agent, AgentGroup, AgentQueueMember}
import xivo.models.XivoObject.{ObjectDefinition, ObjectType}
import xuctest.BaseTest

class UserRightSpec extends BaseTest with MockitoSugar {

  "The Right singleton" should {
    "parse admin right from JSON" in {
      val json = Json.obj(
        "type" -> "admin",
        "data" -> Json.obj()
      )

      UserRight.validate(json).asOpt shouldEqual Some(AdminRight())
    }

    "parse supervisor right from JSON" in {
      val json = Json.obj(
        "type" -> "supervisor",
        "data" -> Json.obj(
          "queueIds" -> List(1, 2),
          "groupIds" -> List(3, 4)
        )
      )

      UserRight.validate(json).asOpt shouldEqual Some(SupervisorRight(List(1, 2), List(3, 4)))
    }
  }

  "A SupervisorRight" should {

    class Helper {
      val (queueId1, queueId2, queueId3) = (4, 78, 6)
      val (groupId1, groupId2) = (7, 14)
      val right = SupervisorRight(List(queueId1, queueId3), List(groupId1))
      ConfigRepository.repo = mock[ConfigRepository]
    }

    "filter QueueList" in new Helper {
      val (q1, q2, q3) = (mock[QueueConfigUpdate], mock[QueueConfigUpdate], mock[QueueConfigUpdate])
      stub(q1.getId).toReturn(queueId1)
      stub(q2.getId).toReturn(queueId2)
      stub(q3.getId).toReturn(queueId3)
      val queueList = QueueList(List(q1, q2, q3))

      right.filter(queueList) shouldEqual Some(QueueList(List(q1, q3)))
    }

    "filter QueueMemberList based on the queue ids" in new Helper {
      val (qm1, qm2, qm3) = (AgentQueueMember(12, queueId1, 0), AgentQueueMember(13, queueId2, 0), AgentQueueMember(14, queueId3, 0))
      val qmList = AgentQueueMemberList(List(qm1, qm2, qm3))
      stub(ConfigRepository.repo.groupIdForAgent(qm1.agentId)).toReturn(Some(groupId1))
      stub(ConfigRepository.repo.groupIdForAgent(qm3.agentId)).toReturn(Some(groupId1))

      right.filter(qmList) shouldEqual Some(AgentQueueMemberList(List(qm1, qm3)))
    }

    "filter QueueMemberList based on the group ids" in new Helper {
      val (qm1, qm2) = (AgentQueueMember(12, queueId1, 0), AgentQueueMember(14, queueId1, 0))
      val qmList = AgentQueueMemberList(List(qm1, qm2))
      stub(ConfigRepository.repo.groupIdForAgent(qm1.agentId)).toReturn(Some(groupId1))
      stub(ConfigRepository.repo.groupIdForAgent(qm2.agentId)).toReturn(Some(groupId2))

      right.filter(qmList) shouldEqual Some(AgentQueueMemberList(List(qm1)))
    }

    "filter AggregatedStatEvent" in new Helper {
      val (stat1, stat2, stat3) = (AggregatedStatEvent(ObjectDefinition(ObjectType.Queue, Some(queueId1)), List()),
        AggregatedStatEvent(ObjectDefinition(ObjectType.Queue, Some(queueId2)), List()),
        AggregatedStatEvent(ObjectDefinition(ObjectType.Queue, Some(queueId3)), List()))

      right.filter(stat1) shouldEqual Some(stat1)
      right.filter(stat2) shouldEqual None
      right.filter(stat3) shouldEqual Some(stat3)
    }

    "filter AgentState" in new Helper {
      val (state1, state2) = (AgentLogin(48, new DateTime, "1234", List(4, 58)), AgentLogin(49, new DateTime, "7895", List(12)))
      stub(ConfigRepository.repo.groupIdForAgent(state1.id)).toReturn(Some(groupId1))
      stub(ConfigRepository.repo.groupIdForAgent(state2.id)).toReturn(Some(groupId2))

      right.filter(state1) shouldEqual Some(state1)
      right.filter(state2) shouldEqual None
    }

    "filter AgentStatistics" in new Helper {
      val stat1 = AgentStatistic(14, List())
      val stat2 = AgentStatistic(15, List())
      stub(ConfigRepository.repo.groupIdForAgent(stat1.agentId)).toReturn(Some(groupId1))
      stub(ConfigRepository.repo.groupIdForAgent(stat2.agentId)).toReturn(Some(groupId2))

      right.filter(stat1) shouldEqual Some(stat1)
      right.filter(stat2) shouldEqual None
    }

    "filter AgentGroupList" in new Helper {
      val list = AgentGroupList(List(AgentGroup(Some(groupId1), "john"), AgentGroup(Some(groupId2), "doe")))
      right.filter(list) shouldEqual Some(AgentGroupList(List(AgentGroup(Some(groupId1), "john"))))
    }

    "filter AgentList" in new Helper {
      val a1 = Agent(25, "John", "Doe", "1000", "default", groupId1)
      val a2 = Agent(26, "Lucky", "Luke", "1002", "default", groupId2)

      right.filter(AgentList(List(a1, a2))) shouldEqual Some(AgentList(List(a1)))
    }
  }

  "An AdminRight" should {
    "return the object unchanged" in {
      val o = "dummy"
      AdminRight().filter(o) shouldEqual Some(o)
    }
  }

}
