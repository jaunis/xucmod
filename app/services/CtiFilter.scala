package services

import akka.actor.{Actor, ActorRef, Props, actorRef2Scala}
import com.fasterxml.jackson.databind.JsonNode
import models.{PhoneState, XucUser}
import org.xivo.cti.MessageFactory
import org.xivo.cti.message._
import org.xivo.cti.model.{PhoneHintStatus, QueueStatRequest}
import play.api.Logger
import services.XucStatsEventBus.AggregatedStatEvent
import services.config.ConfigDispatcher._
import services.config.{LineConfig, OutboundQueueNumber}
import services.request._
import xivo.ami.AmiBusConnector.{AgentListenStarted, AgentListenStopped, LineEvent}
import xivo.events.AgentState
import xivo.events.AgentState.AgentReady
import xivo.models.Agent
import xivo.network.LoggedOn
import xivo.phonedevices.{DeviceAdapter, DeviceAdapterFactory}
import xivo.websocket.WsBus.WsContent
import xivo.websocket.{WSMsgType, WebsocketEvent}

import scala.collection.JavaConversions._


trait CtiFilterLog {
  this: CtiFilter =>
  val logger = Logger(getClass.getPackage.getName + ".CtiFilter." + self.path.name)
  private def logMessage(msg: String): String = s"[${user.ctiUsername}]-${filterConfig.userId.getOrElse("..")}-${user.phoneNumber.getOrElse("..")}-${filterConfig.agentId.getOrElse("..")}-${filterConfig.phoneId.getOrElse("..")} - $msg"
  object log {
    def debug(msg: String) = logger.debug(logMessage(msg))
    def info(msg: String) = logger.info(logMessage(msg))
    def error(msg: String) = logger.error(logMessage(msg))
    def warn(msg: String) = logger.warn(logMessage(msg))
  }
}

object CtiFilter {
  def props(router:ActorRef): Props = Props(new CtiFilter(myRouter=router))
}
case class VoiceMailConfig(id: Long, enabled: Boolean)
object VoiceMailConfig {
  def enabled(id: Long) = VoiceMailConfig(id, true)
  def disabled(id: Long) = VoiceMailConfig(id, false)
}
case class FilterConfig(userId: Option[Long], agentId: Option[Agent.Id], phoneId: Option[Long], outBoundQueueNumber:Option[String], voiceMailConfig : VoiceMailConfig) {
  def withAgentId(agentId : Agent.Id) = this.copy(agentId = Some(agentId))
  def withUserId(userId: Long) = this.copy(userId = Some(userId))
  def withPhoneId(phoneId: Long) = this.copy(phoneId = Some(phoneId))
  def withNoPhoneId = this.copy(phoneId = None)
  def withOutboundQueueNumber(queueNumber: String) = this.copy(outBoundQueueNumber = Some(queueNumber))
  def withNoOutboundQueue = this.copy(outBoundQueueNumber = None)
  def withVoiceMailConfig(voiceMailConfig: VoiceMailConfig) = this.copy(voiceMailConfig=voiceMailConfig)
}
object FilterConfig {
  def apply():FilterConfig = FilterConfig(None,None,None, None,VoiceMailConfig.disabled(0))
}

sealed trait filterEventChecker {
  this:CtiFilter =>
  def eventIsForMe(statusUpdate: UserStatusUpdate):Boolean = statusUpdate.getUserId == filterConfig.userId.getOrElse(-1)
  def eventIsForMe(configUpdate: UserConfigUpdate):Boolean = configUpdate.getUserId == filterConfig.userId.getOrElse(-1)
  def eventIsForMe(phoneStatusUpdate: PhoneStatusUpdate):Boolean = phoneStatusUpdate.getLineId == filterConfig.phoneId.getOrElse(-1)
  def eventIsForMe(voiceMailStatusUpdate: VoiceMailStatusUpdate):Boolean = voiceMailStatusUpdate.getVoiceMailId == filterConfig.voiceMailConfig.id

}

class CtiFilter(val eventBus: XucEventBus = XucEventBus.bus,
  var deviceAdapterFactory: DeviceAdapterFactory = new DeviceAdapterFactory,
  myRouter: ActorRef) extends Actor with filterEventChecker with CtiFilterLog {

  protected[services] var messageFactory = new MessageFactory
  protected[services] var user: XucUser = new XucUser("defaultUserCtiFilter", "")
  protected[services] var filterConfig = FilterConfig()
  protected[services] var enableOutboundDial = false
  protected[services] var deviceAdapter: Option[DeviceAdapter] = None

  log.info(s"starting cti filter ${self.path} ${context.parent}")
  var phoneState = new PhoneState(PhoneHintStatus.UNEXISTING)

  private def updateAgentId(update: UserConfigUpdate) {
    import services.config.ConfigDispatcher.ObjectType.TypeAgent
    if (update.getAgentId != 0) {
      filterConfig = filterConfig.withAgentId(update.getAgentId)
      val agentTopic = XucEventBus.agentEventTopic(update.getAgentId)
      eventBus.unsubscribe(self)
      eventBus.subscribe(self, agentTopic)
      myRouter ! RequestStatus(self, update.getAgentId, TypeAgent)
      log.info(s"agentId  ${update.getAgentId} saved subscribed to $agentTopic")
    }
  }
  private def updatePhoneId(update: UserConfigUpdate) {
    update.getLineIds.toList match {
      case Nil =>
      case phoneId :: tail =>
        filterConfig = filterConfig.withPhoneId(phoneId.toLong)
        log.info(s"Saving used phoneId : ${filterConfig.phoneId}")
        myRouter ! RequestConfig(self, LineConfigQueryById(update.getLineIds.get(0)))
    }
  }

  private def processUserConfigUpdate(userConfigUpdate: UserConfigUpdate) {
    if (eventIsForMe(userConfigUpdate)) {
      updateAgentId(userConfigUpdate)
      updatePhoneId(userConfigUpdate)
      filterConfig = filterConfig.withVoiceMailConfig(VoiceMailConfig(userConfigUpdate.getVoiceMailId, userConfigUpdate.isVoiceMailEnabled))
      if (filterConfig.voiceMailConfig.enabled) myRouter ! messageFactory.createGetVoicemailStatus(filterConfig.voiceMailConfig.id.toString)
      myRouter ! WebsocketEvent.createEvent(userConfigUpdate)
    }
  }

  private def processPhoneStatusUpdate(phoneStatusUpdate: PhoneStatusUpdate) {
    if (eventIsForMe(phoneStatusUpdate)) {
      try {
        logger.debug(phoneStatusUpdate.toString)
        phoneState = PhoneState.getPhoneState(phoneStatusUpdate)
        sendPhoneStatusUpdate(phoneStatusUpdate.getHintStatus)
      } catch {
        case e: NumberFormatException =>
          log.debug("Invalid PhoneHintStatus value, dropping message: " + phoneStatusUpdate)
      }
    }
  }

  private def sendPhoneStatusUpdate(phoneHintStatus: String) {
    val hintStatus = PhoneHintStatus.getHintStatus(Integer.decode(phoneHintStatus))
    log.debug("Sending phoneStatusUpdate message to websocket, status:" + hintStatus)
    myRouter ! WebsocketEvent.createEvent(hintStatus)
  }

  private def processLoggedInOnAnotherPhoneError(state: AgentState) = {
    user.phoneNumber.foreach {
      phoneNb =>
        if (state.phoneNb.length > 0 && state.phoneNb != phoneNb.toString)
          myRouter ! WebsocketEvent.createError(WSMsgType.AgentError, "LoggedInOnAnotherPhone", Map("phoneNb" -> state.phoneNb, "RequestedNb"-> phoneNb.toString))
    }
  }

  private def processWebsocketCommand(message: JsonNode) {
    message.get("command").asText match {

      case "agentLogout" =>
        log.debug(s"CreateAgentLogout websocket command $message")
        if (message.has("agentid"))
          sender ! messageFactory.createAgentLogout(message.get("agentid").asText)
        else
          sender ! messageFactory.createAgentLogout(filterConfig.agentId.getOrElse(0).toString)

      case "pauseAgent" =>
        log.debug(s"websocket command pause agent $message")
        if (message.has("agentid"))
          sender ! messageFactory.createPauseAgent(message.get("agentid").asText)
        else
          sender ! messageFactory.createPauseAgent(filterConfig.agentId.getOrElse(0).toString)

      case "unpauseAgent" =>
        log.debug(s"websocket command unpause agent $message")
        if (message.has("agentid"))
          sender ! messageFactory.createUnpauseAgent(message.get("agentid").asText)
        else
          sender ! messageFactory.createUnpauseAgent(filterConfig.agentId.getOrElse(0).toString)

      case "userStatusUpdate" =>
        log.debug("CreateUserStatusUpdate as asked by the websocket command")
        sender ! messageFactory.createUserAvailState(filterConfig.userId.getOrElse(0).toString, message.get("status").asText)

      case "dnd" =>
        log.debug("websocket command dnd" + message.get("state").asText)
        sender ! messageFactory.createDND(message.get("state").asBoolean())

      case "dial" =>
        val dstNo = message.get("destination").asText
        log.debug("websocket command dial" + dstNo)
        filterConfig.agentId match {
          case Some(id) if enableOutboundDial &&  filterConfig.outBoundQueueNumber.isDefined =>
            log.info(s"Outbound dialing $dstNo")
            filterConfig.outBoundQueueNumber.foreach { queueNb =>
              deviceAdapter.map(_.odial(dstNo, id, queueNb, sender))
            }
          case _ =>
            deviceAdapter.map(_.dial(dstNo, sender))
        }

      case "answer" =>
        log.debug("websocket command answer")
        deviceAdapter.map(_.answer(sender))

      case "hangup" =>
        log.debug("websocket command hangup")
        deviceAdapter.map(_.hangup(sender))

      case "directTransfer" =>
        log.debug("websocket command directTransfer" + message.get("destination").asText)
        sender ! messageFactory.createDirectTransfer(message.get("destination").asText)

      case "attendedTransfer" =>
        log.debug("websocket command attendedTransfer" + message.get("destination").asText)
        deviceAdapter.map(_.attendedTransfer(message.get("destination").asText, sender))

      case "completeTransfer" =>
        log.debug("websocket command completeTransfer")
        deviceAdapter.map(_.completeTransfer(sender))

      case "cancelTransfer" =>
        log.debug("websocket command cancelTransfer")
        deviceAdapter.map(_.cancelTransfer(sender))

      case "conference" =>
        log.debug("websocket command conference")
        deviceAdapter.map(_.conference(sender))

      case "hold" =>
        log.debug("websocket command hold")
        deviceAdapter.map(_.hold(sender))

      case "searchDirectory" =>
        sender ! messageFactory.createSearchDirectory(message.get("pattern").asText)
        log.debug("websocket command searchDirectory" + message.get("pattern").asText)

      case "getQueueStatistics" =>
        val requests = new java.util.ArrayList[QueueStatRequest]()
        val request = new QueueStatRequest(message.get("queueId").asText, message.get("window").asInt, message.get("xqos").asInt)
        requests.add(request)
        sender ! messageFactory.createGetQueueStatistics(requests)

      case "setAgentQueue" =>
        myRouter ! ConfigChangeRequest(self,SetAgentQueue(message.get("agentId").asInt(), message.get("queueId").asInt(), message.get("penalty").asInt()))

      case "removeAgentFromQueue" =>
        myRouter ! ConfigChangeRequest(self,RemoveAgentFromQueue(message.get("agentId").asInt(), message.get("queueId").asInt()))

      case _ =>
        log.warn("***Received unknown command from the websocket: " + message)
    }
  }
  import services.config.ConfigDispatcher.ObjectType.{TypeAgent, TypePhone, TypeUser}
  import xivo.events.AgentState.{AgentLoggedOut, AgentLogin}

  def receive = {

    case Start(newUser) =>
      log.info(s"Start $newUser")
      user = newUser

    case ln: LoggedOn =>
      filterConfig = filterConfig.withUserId(ln.userId.toLong)
      log.info(s"LoggedOn Saving userId: ${ln.userId}")

      sender ! WebsocketEvent.createLoggedOnEvent
      sender ! WebsocketEvent.createEvent(ln.userStatuses)
      sender ! RequestStatus(self, ln.userId.toInt, TypeUser)
      sender ! messageFactory.createGetUserConfig(filterConfig.userId.getOrElse(0).toString)
      if (filterConfig.voiceMailConfig.enabled) sender ! messageFactory.createGetVoicemailStatus(filterConfig.voiceMailConfig.id.toString)

    case ClientConnected(connectedActor,ln) =>
      log.info(s"client connected : ${ln.user} - ${ln.userId} - ${ln.userStatuses.size()} ")
      connectedActor ! WsContent(WebsocketEvent.createLoggedOnEvent)
      connectedActor ! WsContent(WebsocketEvent.createEvent(ln.userStatuses))
      myRouter ! RequestStatus(self, ln.userId.toInt, TypeUser)
      myRouter ! messageFactory.createGetUserConfig(filterConfig.userId.getOrElse(0).toString)
      if (filterConfig.voiceMailConfig.enabled) myRouter ! messageFactory.createGetVoicemailStatus(filterConfig.voiceMailConfig.id.toString)

      filterConfig.agentId.foreach( agentId  => sender ! RequestStatus(self,agentId.toInt, TypeAgent))
      filterConfig.phoneId.foreach( phoneId  => sender ! RequestStatus(self,phoneId.toInt, TypePhone))

    case sheet: Sheet =>
      log.info(s"Sending Sheet : $sheet phone state : $phoneState")
      myRouter ! WebsocketEvent.createEvent(sheet)

    case userStatusUpdate: UserStatusUpdate if (eventIsForMe(userStatusUpdate)) =>
      log.debug(s"Processing UserStatusUpdate message $userStatusUpdate")
      myRouter ! WebsocketEvent.createEvent(userStatusUpdate)

    case userConfigUpdate: UserConfigUpdate =>
      log.debug(s"Processing UserConfigUpdate : $userConfigUpdate")
      processUserConfigUpdate(userConfigUpdate)

    case phoneStatusUpdate: PhoneStatusUpdate =>
      processPhoneStatusUpdate(phoneStatusUpdate)

    case voiceMailStatusUpdate: VoiceMailStatusUpdate if(eventIsForMe(voiceMailStatusUpdate: VoiceMailStatusUpdate)) => myRouter ! WebsocketEvent.createEvent(voiceMailStatusUpdate)

    case queueStatistics: QueueStatistics =>
      log.debug(s"processing queueStatistics : $queueStatistics received from $sender")
      sender !  WebsocketEvent.createEvent(queueStatistics)

    case browserMessage: BrowserMessage => processWebsocketCommand(browserMessage.message)

    case statEvent: AggregatedStatEvent =>
      log.debug(s"stat received : $statEvent")
      myRouter ! WebsocketEvent.createEvent(statEvent)

    case lcf: LineConfig =>
      filterConfig = filterConfig.withPhoneId(lcf.id.toLong)
      lcf.line.foreach { line =>
        deviceAdapter = Some(deviceAdapterFactory.getAdapter(line))
        myRouter ! RequestStatus(self, lcf.id.toInt, TypePhone)
      }
      try {
        eventBus.subscribe(self, XucEventBus.lineEventTopic(lcf.number.toInt))
      } catch {
        case e: NumberFormatException =>
          log.warn(s"Unable to subscribe to Line event because of non string number ${lcf.number}")
      }
      log.info(s"updated : phone id ${lcf.id} number ${lcf.number} line ${lcf.line}")

    case OutboundQueueNumber(nb) =>
      filterConfig = filterConfig.withOutboundQueueNumber(nb)
      log.info(s"Using queue number $nb for outbound calls : $filterConfig")

    case jsonMessage: JsonNode => myRouter ! jsonMessage

    case agentState: AgentState => receiveAgentState(agentState)

    case agentListen: AgentListen => filterConfig.userId match {
      case Some(userId) => myRouter ! agentListen.copy(fromUser = filterConfig.userId)
      case None =>
        log.error("Cannot listen valid userid not found")
        myRouter ! InvalidRequest("Cannot listen valid userid not found", s"$agentListen")
    }

    case AgentLoginRequest(None, Some(phoneNumber), None) =>
      filterConfig.agentId match {
        case Some(agId) => myRouter ! BaseRequest(self,AgentLoginRequest(Some(agId.toLong), Some(phoneNumber)))
        case None=>  myRouter ! WebsocketEvent.createError(WSMsgType.AgentError,s"user ${user.ctiUsername} is not an agent")
      }

    case le: LineEvent =>
      val les = WebsocketEvent.createEvent(le)
      log.debug(s"forwarding line event to the router $les, type ${les.getClass}")
      myRouter ! WebsocketEvent.createEvent(le)

    case agls : AgentListenStarted =>
      myRouter ! WebsocketEvent.createEvent(agls)

    case agls : AgentListenStopped =>
      myRouter ! WebsocketEvent.createEvent(agls)

    case _ =>
  }

  private def receiveAgentState(agentState: xivo.events.AgentState): Unit = {
    log.info(s"$agentState received from $sender")
    myRouter ! WebsocketEvent.createEvent(agentState)

    agentState match {

      case agentLoggedOut: AgentLoggedOut => filterConfig = filterConfig.withNoPhoneId

      case agentLoggedIn: AgentLogin =>
        filterConfig = filterConfig.withNoOutboundQueue
        myRouter ! RequestConfig(self, LineConfigQueryByNb(agentState.phoneNb))
        myRouter ! RequestConfig(self, OutboundQueueNumberQuery(agentLoggedIn.queues))

      case agentReady : AgentReady =>
        enableOutboundDial = true
        if(filterConfig.phoneId isEmpty)  myRouter ! RequestConfig(self, LineConfigQueryByNb(agentState.phoneNb))
        myRouter ! RequestConfig(self, OutboundQueueNumberQuery(agentReady.queues))
        processLoggedInOnAnotherPhoneError(agentState)

      case _ =>
        enableOutboundDial = false
        if(filterConfig.phoneId isEmpty)  myRouter ! RequestConfig(self, LineConfigQueryByNb(agentState.phoneNb))
        processLoggedInOnAnotherPhoneError(agentState)
    }
  }
}
