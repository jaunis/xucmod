package services.line

import akka.actor.{ActorContext, ActorRef}

class XucLineFactory() {
  def create(id: Int, context : ActorContext): ActorRef = context.actorOf(XucLine.props(id),s"Line$id")
}