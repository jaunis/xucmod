package services.request

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, JsValue, Reads}

class AgentConfigRequest extends XucRequest

case class MoveAgentInGroup(groupId:Long, fromQueueId: Long, fromPenalty : Int, toQueueId : Long, toPenalty: Int) extends AgentConfigRequest
object MoveAgentInGroup {
  def validate(json: JsValue) = json.validate[MoveAgentInGroup]

  implicit val MoveAgentInGroupReads: Reads[MoveAgentInGroup] = (
    (JsPath \ "groupId").read[Long] and
      (JsPath \ "fromQueueId").read[Long] and
      (JsPath \ "fromPenalty").read[Int] and
      (JsPath \ "toQueueId").read[Long] and
      (JsPath \ "toPenalty").read[Int]
    )(MoveAgentInGroup.apply _)
}

case class AddAgentInGroup(groupId:Long, fromQueueId: Long, fromPenalty : Int, toQueueId : Long, toPenalty: Int) extends AgentConfigRequest
object AddAgentInGroup {
  def validate(json: JsValue) = json.validate[AddAgentInGroup]

  implicit val AddAgentInGroupReads: Reads[AddAgentInGroup] = (
    (JsPath \ "groupId").read[Long] and
      (JsPath \ "fromQueueId").read[Long] and
      (JsPath \ "fromPenalty").read[Int] and
      (JsPath \ "toQueueId").read[Long] and
      (JsPath \ "toPenalty").read[Int]
    )(AddAgentInGroup.apply _)
}

case class RemoveAgentGroupFromQueueGroup(groupId:Long, queueId: Long, penalty: Int) extends AgentConfigRequest

object RemoveAgentGroupFromQueueGroup {
  def validate(json: JsValue) = json.validate[RemoveAgentGroupFromQueueGroup]

  implicit val RemoveAgentGroupFromQueueGroupReads: Reads[RemoveAgentGroupFromQueueGroup] = (
    (JsPath \ "groupId").read[Long] and
      (JsPath \ "queueId").read[Long] and
      (JsPath \ "penalty").read[Int]
    )(RemoveAgentGroupFromQueueGroup.apply _)

}

case class AddAgentsNotInQueueFromGroupTo(groupId:Long, queueId: Long, penalty: Int) extends AgentConfigRequest

object AddAgentsNotInQueueFromGroupTo {
  def validate(json: JsValue) = json.validate[AddAgentsNotInQueueFromGroupTo]

  implicit val AddAgentsNotInQueueFromGroupToReads: Reads[AddAgentsNotInQueueFromGroupTo] = (
    (JsPath \ "groupId").read[Long] and
      (JsPath \ "queueId").read[Long] and
      (JsPath \ "penalty").read[Int]
    )(AddAgentsNotInQueueFromGroupTo.apply _)

}

case class SetAgentGroup(agentId: Long, groupId: Long) extends AgentConfigRequest

object SetAgentGroup {
  def validate(json: JsValue) = json.validate[SetAgentGroup]

  implicit val setAgentGroupRead: Reads[SetAgentGroup] = (
    (JsPath \ "agentId").read[Long] and
    (JsPath \ "groupId").read[Long])(SetAgentGroup.apply _)
}