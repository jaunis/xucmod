package services.agent

import akka.actor._
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.IpbxCommandResponse
import services.XucAmiBus.{ListenActionRequest, ListenRequest}
import services.{XucEventBus, ProductionActorFactory, ActorFactory}
import services.config.ConfigRepository
import services.request._
import xivo.events.AgentError
import xivo.events.AgentState.{AgentLoggedOut, AgentOnCall, AgentOnPause, AgentReady}
import xivo.models.Agent
import xivo.models.Agent.Id
import xivo.websocket.WebsocketEvent
import xivo.xuc.api.{RequestSuccess, RequestError}

object AgentAction {
  def props(ctiLink: ActorRef) = Props(new AgentAction(ctiLink) with ProductionActorFactory)
}


class AgentAction(ctiLink : ActorRef,
                  messageFactory: MessageFactory = new MessageFactory,
                   configRepo: ConfigRepository = ConfigRepository.repo,
                   eventBus: XucEventBus = XucEventBus.bus) extends Actor with ActorLogging {
  this: ActorFactory =>
  log.info(s"Agent action $self starting")

  def receive = {

    case AgentListen(agentId, Some(userId)) =>
      log.debug(s"request listen $agentId $userId")
      for {
        listener <- configRepo.getLineForUser(userId)
        listened <- configRepo.getLineForAgent(agentId)
      } {
        log.debug(s"request listen $listener $listened")
        context.actorSelection(amiBusConnectorURI) ! ListenRequest(ListenActionRequest(listener.interface, listened.interface))
      }

    case AgentLoginRequest(Some(agentId), Some(phoneNumber), None) =>
      log.debug(s"login request $agentId, $phoneNumber")
      manageOtherAgentAlreadyLoggedOn(agentId, phoneNumber)
      ctiLink ! messageFactory.createAgentLogin(agentId.toString, phoneNumber)
    case AgentLoginRequest(Some(agentId), None, None) => ctiLink ! messageFactory.createAgentLogin(agentId.toString, "")
    case AgentLoginRequest(None,Some(phoneNumber), None) =>
      ctiLink ! messageFactory.createAgentLogin(phoneNumber)

    case BaseRequest(requester, request: AgentLoginRequest) =>
      def subscribeAndForwardItself(msg: AgentLoginRequest) {
        log.debug("Subscribing requester to agent's events")
        eventBus.subscribe(requester, XucEventBus.allAgentsEventsTopic)
        self ! msg
      }
      def processAgentState(agentId: Id, msg: AgentLoginRequest) {
        configRepo.getAgentState(agentId) match {
          case Some(agentState) =>
            log.debug(s"AgentId: $agentId agentState: $agentState")
            if (agentState.getClass != classOf[AgentLoggedOut]) {
              requester ! RequestSuccess("Already logged in")
            }
            else {
              log.debug(s"Requesting agent login for agent: $agentId")
              subscribeAndForwardItself(msg)
            }
          case None =>
            log.debug(s"Requesting agent login for agent: $agentId")
            subscribeAndForwardItself(msg)
        }
      }
      request match {
        case AgentLoginRequest(None, Some(phoneNumber), None) =>
          log.debug(s"Requesting agent login for phoneNumber: $phoneNumber")
          subscribeAndForwardItself(request)
        case AgentLoginRequest(Some(agentId), _, _) =>
          configRepo.getAgent(agentId) match {
            case None =>
              requester ! RequestError(s"Agent with agenId: $agentId does not exist")
            case Some(agent) =>
              processAgentState(agentId, request)
          }
        case AgentLoginRequest(None, Some(phoneNb), Some(agentNb)) =>
          configRepo.getAgent(agentNb) match {
            case None =>
              requester ! RequestError(s"Agent with agenNb: $agentNb does not exist")
            case Some(agent) =>
              val msg = AgentLoginRequest(Some(agent.id), Some(phoneNb), None)
              processAgentState(agent.id, msg)
          }
        case any =>
          requester ! RequestError("Invalid parameters")
      }

    case BaseRequest(requester, AgentLogout(phoneNb)) =>
      log.debug(s"logout agent request for phone: $phoneNb")
      val agentId = configRepo.getAgentLoggedOnPhoneNumber(phoneNb)
      agentId match {
        case Some(id) =>
          log.debug(s"Request logout for agent id: $id")
          log.debug("Subscribing requester to agent's events")
          eventBus.subscribe(requester, XucEventBus.agentEventTopic(id))
          ctiLink ! messageFactory.createAgentLogout(id.toString)
        case None =>
          requester ! RequestError("Requested agentLogout for a phone number where's no agent logged")
      }

    case BaseRequest(_, AgentTogglePause(phoneNb)) =>
      log.debug(s"toggle pause request $phoneNb")
      configRepo.getAgentLoggedOnPhoneNumber(phoneNb).foreach(agentId =>{
        log.debug(s"toggle pause request on $phoneNb found agent $agentId")
        configRepo.getAgentState(agentId) match {
          case Some(ready : AgentReady) =>
            log.debug(s"$ready")
            ctiLink ! messageFactory.createPauseAgent(agentId.toString)
          case Some(onCall: AgentOnCall) if !onCall.onPause =>
            log.debug(s"$onCall on pause")
            ctiLink ! messageFactory.createPauseAgent(agentId.toString)
          case Some(onCall: AgentOnCall) if onCall.onPause =>
            log.debug(s"$onCall ready")
            ctiLink ! messageFactory.createUnpauseAgent(agentId.toString)
          case Some(onPause : AgentOnPause) =>
            log.debug(s"$onPause")
            ctiLink ! messageFactory.createUnpauseAgent(agentId.toString)
          case Some(agentState) => log.error(s"Not processed agent in state : $agentState")
          case uk => log.debug(s"unknown : $uk")
        }
      })

    case agentRequestError : IpbxCommandResponse =>
      sender ! WebsocketEvent.createEvent(agentRequestError)

    case e: AgentError =>
      log.debug(s"Publishing agentError to the eventBus ($e)")
      eventBus.publish(e)

    case unkown => log.debug(s"unkown $unkown message received")
  }

  private def manageOtherAgentAlreadyLoggedOn(agentId: Agent.Id, phoneNumber : String) = {
    configRepo.getAgentLoggedOnPhoneNumber(phoneNumber).map(
      { loggedId =>
        if (loggedId != agentId) {
          log.info(s"Request login : other agent $loggedId already logged on $phoneNumber : sending logout request")
          ctiLink ! messageFactory.createAgentLogout(loggedId.toString)
        }
      }
    )
  }
}
