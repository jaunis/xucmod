package services.config

import xivo.ami.AgentCallUpdate
import xivo.events.AgentState.AgentLogin
import xivo.models.Agent
import scala.collection.mutable.{ Map => MMap }
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import services.AgentActorFactory
import xivo.events.AgentState

object AgentManager {
  def props(): Props = Props(new AgentManager(new AgentActorFactory))
}

class AgentManager(factory: AgentActorFactory) extends Actor with ActorLogging {

  log.info(s"Starting AgentManager ${self} ")

  val agFsms: MMap[Agent.Id, ActorRef] = MMap()

  def receive = {
    case agl : AgentLogin =>
      log.debug(s"creating agent actors for ${agl.id}")
      factory.getOrCreate(agl.id, context)  ! agl
      factory.getOrCreateAgentStatCollector(agl.id, context)  ! agl

    case agentState: AgentState => factory.getOrCreate(agentState.agentId, context) ! agentState

    case agentUserStatus: AgentUserStatus =>
      log.debug(s"$agentUserStatus")
      factory get (agentUserStatus.agentId) foreach (_ ! agentUserStatus)

    case agentCallUpdate: AgentCallUpdate =>
      factory get agentCallUpdate.agentId match {
        case Some(actor) =>
          actor ! agentCallUpdate
        case None =>
          log.warning(s"Received channel update for unknown agent: $agentCallUpdate")
      }


    case unknown => log.debug(s"Uknown message received: $unknown")
  }

}

