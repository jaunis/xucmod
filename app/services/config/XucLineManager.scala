package services.config

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import services.line.XucLineFactory
import xivo.ami.AmiBusConnector.{AgentListenStopped, AgentListenStarted, LineEvent}

import scala.collection.mutable.{Map => MMap}

case class CreateLine(number: Int)

object XucLineManager {
  def props(): Props = Props(new XucLineManager(new XucLineFactory))
}

class XucLineManager(factory: XucLineFactory) extends Actor with ActorLogging {

  log.info(s"Starting Endpoint manager ${self} ")

  val lines: MMap[Int, ActorRef] = MMap()

  def receive = {
    case event: LineEvent =>
      lines get (event.lineNumber) match {
        case Some(actor) =>
          log.debug(s"Forwarding $event to the line actor")
          actor ! event
        case None =>
          log.debug(s"Discarding event ($event) for unexisting line.")
      }

    case AgentListenStarted(phoneNumber, Some(agentId)) =>
      log.debug(s"agent listen started $phoneNumber $agentId")
      lines get(phoneNumber.toInt) map ( xucLine => xucLine ! AgentListenStarted(phoneNumber, Some(agentId)))

    case AgentListenStopped(phoneNumber, Some(agentId)) =>
      lines get(phoneNumber.toInt) map ( xucLine => xucLine ! AgentListenStopped(phoneNumber, Some(agentId)))

    case CreateLine(lineNumber) =>
      lines get (lineNumber) match {
        case Some(actor) =>
          log.info(s"line actor $lineNumber already created")
        case None =>
          log.info(s"creating actor line for number $lineNumber")
          val line = factory.create(lineNumber, context)
          lines += (lineNumber -> line)
      }

    case unknown => log.debug(s"Uknown message received: $unknown")
  }

}

