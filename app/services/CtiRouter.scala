package services

import akka.actor.ActorSelection.toScala
import akka.actor.{Actor, ActorRef, PoisonPill, Props, Terminated, actorRef2Scala}
import com.codahale.metrics.SharedMetricRegistries
import com.fasterxml.jackson.databind.JsonNode
import models.XucUser
import org.json.JSONObject
import org.xivo.cti.message.{DirectoryResult, IpbxCommandResponse}
import org.xivo.cti.{CtiMessage, MessageFactory}
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import services.XucEventBus.{Topic, TopicType}
import services.agent.AgentAction
import services.request._
import services.user.{CtiUserAction, UserAction}
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.XivoObject.ObjectType._
import xivo.network.CtiLinkKeepAlive.StartKeepAlive
import xivo.network.{CtiLinkKeepAlive, LoggedOn}
import xivo.phonedevices.RequestToAmi
import xivo.websocket.WsActor.WsConnected
import xivo.websocket.WsBus._
import xivo.websocket._
import xivo.xuc.XucConfig

import scala.concurrent.duration.DurationInt

trait RouterLog {
  this: CtiRouter =>
  val logger = Logger(getClass.getPackage().getName() + ".CtiRouter." + self.path.name)
  private def logMessage(msg: String): String = s"[${user.ctiUsername}] $msg"
  object log {
    def debug(msg: String) = logger.debug(logMessage(msg))
    def info(msg: String) = logger.info(logMessage(msg))
    def error(msg: String) = logger.error(logMessage(msg))
    def warn(msg: String) = logger.warn(logMessage(msg))
  }
}

object CtiRouter {
  def props(user: XucUser): Props =
    Props(new CtiRouter(user) with ProductionActorFactory with CtiUserAction)
}

class CtiRouter(protected[services] var user: XucUser,
                bus : WsBus = WsBus.bus,
                eventBus: XucEventBus = XucEventBus.bus,
                statEventBus: XucStatsEventBus = XucStatsEventBus.statEventBus,
                val messageFactory: MessageFactory = new MessageFactory()) extends Actor with RouterLog {
  this: ActorFactory with UserAction =>

  val registry = SharedMetricRegistries.getOrCreate(XucConfig.metricsRegistryName)
  val nbOfClients = registry.counter(s"${user.ctiUsername}-router-nbOfClients")
  val totalClients = registry.counter("global.router.nbOfClients")

  protected[services] var userLoggedOn = false
  protected[services] var loggedOnMsg: Option[LoggedOn] = None

  protected[services] var creator: ActorRef = null
  protected[services] var ctiLink: ActorRef = null
  protected[services] var ctiFilter: ActorRef = null
  protected[services] var agentActionService: ActorRef = null
  protected[services] var ctiKeepAlive: ActorRef = null

  log.info(s"Starting CtiRouter actor $ctiLink")

  override def preStart() = {

    ctiFilter = getFilter(user,self)

    log.info(s"Creating link actor")
    ctiLink = getCtiLink(user.ctiUsername)
    context.watch(ctiLink)
    log.info(s"Creating agent action service")
    agentActionService = context.actorOf(AgentAction.props(ctiLink))
    ctiKeepAlive = context.actorOf(CtiLinkKeepAlive.props(user.ctiUsername))
    log.debug(s"Actor $ctiLink created")
  }

  private def onClientConnected(actor: ActorRef, newUser: XucUser) = {
    nbOfClients.inc()
    totalClients.inc()
    creator = actor
    ctiFilter ! Start(newUser)
    user = newUser
    log.info(s"Connected ${user.ctiUsername} : ${nbOfClients.getCount} client(s) connected")

  }
  import services.config.ConfigDispatcher._
  def receive = {

    case Start(user) =>
      onClientConnected(sender, user)
      loggedOnMsg.foreach(ctiFilter ! _)

    case WsConnected(wsActor, wsActorUser) =>
      onClientConnected(wsActor, wsActorUser)
      loggedOnMsg.foreach(ctiFilter ! ClientConnected(wsActor, _))

    case terminated: Terminated =>
      terminated.getActor match {
        case actor: ActorRef if (actor == ctiLink) => {
          userLoggedOn = false
          log.info("Cti link is terminated ----------------" + terminated.getActor.toString)
          ctiFilter ! PoisonPill
          context stop self
        }

        case _ =>
          log.error("Unknown actor terminated: " + terminated.getActor.path.name)
      }

    case ln: LoggedOn =>
      log.info(s"loggedOn ${ln}")
      loggedOnMsg = Some(ln)
      userLoggedOn = true
      creator ! Started()
      ctiFilter ! ln
      ctiKeepAlive ! StartKeepAlive(ln.userId, ctiLink)

    case directoryResult: DirectoryResult => context.actorSelection(configDispatcherURI) ! directoryResult

    case agentError: IpbxCommandResponse => agentActionService ! agentError

    case message: CtiMessage =>  ctiFilter ! message

    case ctiRequest: JSONObject =>
      if (userLoggedOn) {
        log.debug(s"Sending request ${ctiRequest}")
        ctiLink ! ctiRequest
        sender ! CtiReqSuccess()
      } else {
        log.debug(s"not connected waiting ")
        rescheduleRequest(ctiRequest, sender)
      }

    case jsonMessage: JsonNode =>
      log.debug(">>>to Browser " + jsonMessage.toString)
      bus.publish(WsMessageEvent(WsBus.browserTopic(user.ctiUsername),WsContent(jsonMessage)))

    case retry: CtiReqRetry =>
      if (userLoggedOn) {
        log.debug(s"processing request retry for sender : ${retry.requestSender}")
        ctiLink ! retry.ctiRequest
        retry.requestSender ! CtiReqSuccess()
      } else {
        log.debug(s"unable to process request retry")
        retry.requestSender ! CtiReqError()
      }

    case LinkStatusUpdate(LinkState.up) => ctiLink ! Start(user)

    case LinkStatusUpdate(LinkState.down) =>
      loggedOnMsg = None
      bus.publish(WsMessageEvent(WsBus.browserTopic(user.ctiUsername),WsContent(WebsocketEvent.createEvent(LinkStatusUpdate(LinkState.down)))))

    case Leave(leaving) =>
      nbOfClients.dec()
      totalClients.dec()
      log.info(s"client disconnecting : ${nbOfClients.getCount} client(s) connected total: ${totalClients.getCount}")

    case BaseRequest(_, msg : BrowserMessage) => ctiFilter ! msg

    case BaseRequest(sender,msg : AgentConfigRequest) => context.actorSelection(agentConfigURI) ! BaseRequest(sender,msg)

    case BaseRequest(_, AgentLoginRequest(None,Some(phoneNumber),None)) =>
      log.debug(s"Agent login request, None, $phoneNumber")
      ctiFilter ! AgentLoginRequest(None,Some(phoneNumber))

    case AgentListen(agentId,Some(userId)) =>
      log.debug(s"${AgentListen(agentId, Some(userId))}")
      agentActionService ! AgentListen(agentId, Some(userId))
    case BaseRequest(_, msg: AgentListen) =>
      log.debug(s"AgentListen $msg")
      ctiFilter ! msg
    case BaseRequest(_, msg: AgentActionRequest) =>
      log.debug(s"AgentActionRequest $msg")
      agentActionService ! msg

    case BaseRequest(requester, GetConfig(objectType)) =>
      context.actorSelection(configDispatcherURI) ! RequestConfig(requester, GetConfig(objectType))
      eventBus.subscribe(requester, XucEventBus.configTopic(objectType))

    case BaseRequest(requester, GetList(objectType)) =>
      context.actorSelection(configDispatcherURI) ! RequestConfig(requester, GetList(objectType))
      eventBus.subscribe(requester, XucEventBus.configTopic(objectType))

    case BaseRequest(requester, SubscribeToAgentEvents) =>
      log.debug(s"$requester subscribing to agent events")
      eventBus.subscribe(requester, Topic(TopicType.Event, ObjectType.TypeAgent))

    case BaseRequest(requester, SubscribeToQueueStats) => statEventBus.subscribe(requester, ObjectDefinition(Queue))

    case BaseRequest(requester, SubscribeToAgentStats) =>
      log.debug("subscribing to agent stats")
      context.actorSelection(configDispatcherURI) !  RequestConfig(requester, GetAgentStatistics)
      eventBus.subscribe(requester, Topic(TopicType.Stat, ObjectType.TypeAgent))

    case BaseRequest(requester, SubscribeToQueueCalls(queueId)) =>
      log.debug(s"Subscribing to queue calls for queue $queueId")
      context.actorSelection(configDispatcherURI) ! RequestConfig(requester, GetQueueCalls(queueId))
      eventBus.subscribe(requester, XucEventBus.queueCallsTopic(queueId))

    case BaseRequest(requester, UnSubscribeToQueueCalls(queueId)) =>
      log.debug(s"Unsubscribing to queue calls for queue $queueId")
      eventBus.unsubscribe(requester, XucEventBus.queueCallsTopic(queueId))

    case BaseRequest(requester, configQuery : ConfigQuery) => context.actorSelection(configDispatcherURI) ! RequestConfig(requester, configQuery)

    case BaseRequest(requester, monitorAction : MonitorActionRequest) => context.actorSelection(amiBusConnectorURI) ! monitorAction

    case BaseRequest(_, invite: InviteConferenceRoom) => ctiLink ! messageFactory.createInviteConferenceRoom(invite.userId)

    case BaseRequest(_, forwardRequest: ForwardRequest) => forward(forwardRequest)

    case BaseRequest(ref, GetAgentCallHistory(size)) =>
      logger.debug(s"Forwarding call history request : size = $size to history manager")
      context.actorSelection(callHistoryManagerURI) ! BaseRequest(ref, AgentCallHistoryRequest(size, user.ctiUsername))

    case BaseRequest(ref, GetUserCallHistory(size)) =>
      logger.debug(s"Forwarding call history request : size = $size to history manager")
      context.actorSelection(callHistoryManagerURI) ! BaseRequest(ref, UserCallHistoryRequest(size, user.ctiUsername))

    case requestConfig: RequestConfig => context.actorSelection(configDispatcherURI) ! requestConfig

    case requestStatus: RequestStatus => context.actorSelection(configDispatcherURI) ! requestStatus

    case configChangeRequest : ConfigChangeRequest => context.actorSelection(configDispatcherURI) ! configChangeRequest

    case requestToAmi : RequestToAmi => context.actorSelection(amiBusConnectorURI) ! requestToAmi

    case unknown => log.debug(s"Unknown message received: $unknown from: $sender")

  }

  private def rescheduleRequest(ctiRequest: JSONObject, requestSender: ActorRef) = {
    context.system.scheduler.scheduleOnce(800 milliseconds) {
      self ! CtiReqRetry(ctiRequest, requestSender)
    }
  }
}

case class Start(user: XucUser)
case class Leave(leaving: ActorRef)
case class Started()
case class CtiReqRetry(ctiRequest: JSONObject, requestSender: ActorRef)
case class CtiReqSuccess()
case class CtiReqError()
case class ClientConnected(actor: ActorRef, ln : LoggedOn)
case class BrowserMessage(message: JsonNode) extends XucRequest
