package services

import akka.event.ActorEventBus
import akka.event.SubchannelClassification
import akka.util.Subclassification
import models.QueueCallList
import services.XucEventBus.TopicType.TopicType
import services.XucEventBus.{Topic, XucEvent}
import services.agent.{AgentTransition, AgentStatistic}
import services.config.ConfigDispatcher.ObjectType
import services.config.ConfigDispatcher.ObjectType._
import xivo.ami.AmiBusConnector.{AgentListenNotification, LineEvent}
import xivo.events.{AgentError, AgentState}
import xivo.models.Agent

object XucEventBus {
  val bus = new XucEventBus

  def configTopic(objectType: ObjectType) =   Topic(TopicType.Config, objectType)

  def allAgentsEventsTopic =                    Topic(TopicType.Event, ObjectType.TypeAgent)
  def agentEventTopic(agentId: Agent.Id) =      Topic(TopicType.Event, ObjectType.TypeAgent, Some(agentId))
  def lineEventTopic(lineNumber: Long) =        Topic(TopicType.Event, ObjectType.TypeLine, Some(lineNumber))
  def statEventTopic(agentId: Agent.Id) =       Topic(TopicType.Stat, ObjectType.TypeAgent, Some(agentId))
  def allAgentsStatsTopic =                     Topic(TopicType.Stat, ObjectType.TypeAgent)
  def agentTransitionTopic(agentId: Agent.Id) = Topic(TopicType.Transition, ObjectType.TypeAgent, Some(agentId))

  def queueCallsTopic(queueId: Long) = Topic(TopicType.Event, ObjectType.TypeQueue, Some(queueId))

  case class XucEvent(val topic: Topic, val message: Any)

  object TopicType extends Enumeration {
    type TopicType = Value
    val Event = Value
    val Transition = Value
    val Config = Value
    val Stat = Value
  }
  case class Topic(topicType: TopicType, objectType: ObjectType, objectId: Option[Long] = None)
}

class XucEventBus extends ActorEventBus with SubchannelClassification {

  type Event = XucEvent
  type Classifier = Topic

  protected def subclassification = new Subclassification[Classifier] {
    def isEqual(x: Classifier, y: Classifier) = x == y
    def isSubclass(x: Classifier, y: Classifier) = {
      x.topicType == y.topicType && x.objectType == y.objectType && (x.objectId != None && y.objectId == None) || x == y
    }
  }

  override def classify(event: Event) = event.topic

  protected def publish(event: Event, subscriber: Subscriber) =   subscriber ! event.message

  def publish(agentState: AgentState):Unit =
    publish(XucEvent(XucEventBus.agentEventTopic(agentState.agentId), agentState))

  def publish(agentError: AgentError):Unit =
    publish(XucEvent(XucEventBus.allAgentsEventsTopic, agentError))

  def publish(lineEvent: LineEvent): Unit =
    publish(XucEvent(XucEventBus.lineEventTopic(lineEvent.lineNumber), lineEvent))

  def publish(agentListenStarted: AgentListenNotification): Unit =
    publish(XucEvent(XucEventBus.lineEventTopic(agentListenStarted.phoneNumber.toLong), agentListenStarted))

  def publish(agentStatistic: AgentStatistic): Unit =
    publish(XucEvent(XucEventBus.statEventTopic(agentStatistic.agentId), agentStatistic))

  def publish(agentTransition: AgentTransition, agentId: Agent.Id):Unit =
    publish(XucEvent(XucEventBus.agentTransitionTopic(agentId), agentTransition))

  def publish(queueCalls: QueueCallList): Unit =
    publish(XucEvent(XucEventBus.queueCallsTopic(queueCalls.queueId), queueCalls))

  def publish(agent: Agent): Unit = publish(XucEvent(XucEventBus.agentEventTopic(agent.id), agent))
}

