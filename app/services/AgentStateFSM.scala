package services

import akka.actor.{Actor, ActorRef, FSM, LoggingFSM, Props}
import models.XucUser
import org.xivo.cti.message.UserStatusUpdate
import services.AgentStateFSM._
import services.agent.{ AgentStateProdTHandler, AgentStateTHandler}
import services.config.AgentUserStatus
import services.line.LineState
import xivo.ami.AgentCallUpdate
import xivo.ami.AmiBusConnector.{CallData, LineEvent}
import xivo.events.AgentState
import xivo.events.AgentState._
import xivo.xucami.models.MonitorState
import xivo.xucami.models.MonitorState.MonitorState

object AgentStateFSM {
  def props: Props = Props(new AgentStateFSM(XucEventBus.bus) with AgentStateProdTHandler)

  final case class UserStateEvent(statusUpdate: UserStatusUpdate, user: XucUser)

  case class GetLastReceivedState(requester: ActorRef)

  sealed trait MContext
  case object MEmptyContext extends MContext
  case class AgentStateContext(state: AgentState, userStatus: String = "",
                               monitorState: MonitorState = MonitorState.UNKNOWN,
                               callId: String = "", phoneNumber: String) extends MContext {
    def withState(newState:AgentState): AgentStateContext = this.copy(state=newState)
    def withMonitorState(newMonitorState: MonitorState) = this.copy(monitorState=newMonitorState)
    def withCallId(newCallId:String) = this.copy(callId=newCallId)

  }

  sealed trait MAgentStates

  case object MInitState extends MAgentStates
  case object MAgentLogin extends MAgentStates
  case object MAgentReady extends MAgentStates
  case object MAgentPaused extends MAgentStates
  case object MAgentLoggedOut extends MAgentStates
  case object MAgentOnCall extends MAgentStates
  case object MAgentOnCallOnPause extends  MAgentStates
  case object MAgentRinging extends MAgentStates
  case object MAgentRingingOnPause extends  MAgentStates
  case object MAgentDialing extends MAgentStates
  case object MAgentDialingOnPause extends MAgentStates
  case object MAgentOnWrapup extends MAgentStates


}
trait AgentStateCanGo extends FSM[MAgentStates, MContext] {
  import xivo.events.AgentState.{AgentLoggedOut, AgentOnCall, AgentOnPause, AgentOnWrapup, AgentReady}

  def canGotoLogin:StateFunction = {
    case Event(evtLogin:AgentLogin, _) =>
      goto(MAgentLogin) using  AgentStateContext(state=evtLogin, phoneNumber = evtLogin.phoneNb)
  }
  def canGotoReady:StateFunction = {
    case Event(eventReady: AgentReady, context: AgentStateContext) =>
      goto(MAgentReady) using context.withState(eventReady)
    case Event(eventReady: AgentReady, _) =>
      goto(MAgentReady) using AgentStateContext(eventReady, phoneNumber = eventReady.phoneNb)
  }
  def canGoToPause: StateFunction = {
    case Event(agentOnPause: AgentOnPause, context: AgentStateContext) =>
      goto(MAgentPaused) using context.withState(agentOnPause)
    case Event(agentOnPause: AgentOnPause, _) =>
      goto(MAgentPaused) using AgentStateContext(agentOnPause, phoneNumber = agentOnPause.phoneNb)
  }
  def canGoToLoggout: StateFunction =  {
    case Event(state: AgentLoggedOut, context: AgentStateContext) =>
      goto(MAgentLoggedOut) using AgentStateContext(state, context.userStatus, phoneNumber = context.phoneNumber)
    case Event(state: AgentLoggedOut, _) =>
      goto(MAgentLoggedOut) using AgentStateContext(state, phoneNumber ="")
  }
  def canGoToWrapup: StateFunction = {
    case Event(state: AgentOnWrapup, context: AgentStateContext) =>
      goto(MAgentOnWrapup) using context.withState(state)
  }
  def canGoToIncomingCall: StateFunction = {
    case Event(LineEvent(lineNb, CallData(callId, LineState.UP, callerId, variables)), context: AgentStateContext)
      if (variables.get("XIVO_QUEUENAME").isEmpty) && (lineNb.toString.equals(callerId)) =>
      goto(MAgentOnCall) using AgentStateContext(AgentOnCall(context.state,CallDirection.Incoming, acdState = false, paused=false), context.userStatus, context.monitorState, callId, phoneNumber= context.phoneNumber)
    case Event(LineEvent(lineNb, CallData(callId, LineState.UP, callerId, _ )), context: AgentStateContext)
      if (lineNb.toString.equals(callerId)) =>
      goto(MAgentOnCall) using AgentStateContext(AgentOnCall(context.state,CallDirection.Incoming, acdState = true, paused=false), context.userStatus, context.monitorState, callId, phoneNumber= context.phoneNumber)
  }
  def canGoToDirectOutgoingCall: StateFunction = {
    case Event(LineEvent(_, CallData(callId, LineState.UP, phoneNb, variables)), context: AgentStateContext) if (variables.get("XUC_OUTBOUND") == Some("OtherEnd") && variables.get("XUC_CALLTYPE").isEmpty && phoneNb != context.phoneNumber)  =>
      stay()
    case Event(LineEvent(_, CallData(callId, LineState.UP, phoneNb, variables)), context: AgentStateContext) if (variables.get("XUC_CALLTYPE") == Some("OutboundOriginate") && phoneNb == context.phoneNumber)  =>
      stay()
    case Event(LineEvent(phoneNb, CallData(callId, LineState.UP, callerIdNum, variables)), context: AgentStateContext) if(variables.get("XUC_OUTBOUND").isEmpty && variables.get("XUC_CALLTYPE").isEmpty) =>
      goto(MAgentOnCall) using AgentStateContext(AgentOnCall(context.state,CallDirection.Outgoing, acdState = false, paused=false), context.userStatus, context.monitorState, callId, phoneNumber= context.phoneNumber)
  }
  def canGoToOriginateOutboundCall: StateFunction = {
    case Event(LineEvent(agentPhoneNumber, CallData(callId, LineState.UP, phoneNb , variables)), context: AgentStateContext) if variables.get("XUC_OUTBOUND") == Some("OtherEnd") && variables.get("XUC_CALLTYPE") == Some("OutboundOriginate") && phoneNb != context.phoneNumber =>
      goto(MAgentOnCall) using AgentStateContext(AgentOnCall(context.state,CallDirection.Outgoing, acdState = true, paused=false), context.userStatus, context.monitorState, callId, phoneNumber= context.phoneNumber)
    case Event(LineEvent(agentPhoneNumber, CallData(callId, LineState.UP, phoneNb , variables)), context: AgentStateContext) if variables.get("XUC_CALLTYPE") == Some("Originate") && variables.get("XUC_OTHER_END_UP").isDefined =>
      goto(MAgentOnCall) using AgentStateContext(AgentOnCall(context.state,CallDirection.Outgoing, acdState = false, paused=false), context.userStatus, context.monitorState, callId, phoneNumber= context.phoneNumber)
  }
  def canGoToRinging: StateFunction = {
    case Event(LineEvent(phoneNb, CallData(callId, LineState.RINGING, callerIdNum , variables)), context: AgentStateContext) if variables.get("XUC_CALLTYPE").isEmpty && callerIdNum == context.phoneNumber=>
      goto(MAgentRinging) using context.withState(AgentRinging(context.state))
  }
  def canGoToRingingOnPause: StateFunction = {
    case Event(LineEvent(phoneNb,CallData(callId,LineState.RINGING, callerIdNum, _ )), context:AgentStateContext) if callerIdNum == context.phoneNumber =>
      goto(MAgentRingingOnPause) using context.withState(AgentRinging(context.state))
  }
  def canGoToDialing: StateFunction = {
    case Event(LineEvent(phoneNb, CallData(callId, LineState.ORIGINATING, _, variables )), context: AgentStateContext) =>
      goto(MAgentDialing)  using context.withState(AgentDialing(context.state))
    case Event(LineEvent(phoneNb, CallData(callId, LineState.RINGING, _, variables)), context: AgentStateContext) if variables.get("XUC_CALLTYPE") == Some("OutboundOriginate") =>
      goto(MAgentDialing)  using context.withState(AgentDialing(context.state))
    case Event(LineEvent(phoneNb, CallData(callId, LineState.RINGING, _, variables)), context: AgentStateContext) if variables.get("XUC_CALLTYPE") == Some("Originate") =>
      goto(MAgentDialing)  using context.withState(AgentDialing(context.state))
  }
  def canGoToDialingOnPause: StateFunction = {
    case Event(LineEvent(phoneNb, CallData(callId, LineState.ORIGINATING, _, _ )), context: AgentStateContext) =>
      goto(MAgentDialingOnPause) using context.withState(AgentDialing(context.state))
  }
  def canGoToRingingOnPauseFromRinging: StateFunction = {
    case Event(state: AgentOnPause, context: AgentStateContext) =>
      goto(MAgentRingingOnPause)
  }
  def canGoToRingingFromRingingOnPause: StateFunction = {
    case Event(state: AgentReady, context: AgentStateContext) =>
      goto(MAgentRinging)
  }
  def canGoToDialingOnPauseFromDialing: StateFunction = {
    case Event(state: AgentOnPause, context: AgentStateContext) =>
      goto(MAgentDialingOnPause)
  }
  def canGoToDialingFromDialingOnPause: StateFunction = {
    case Event(state: AgentReady, context: AgentStateContext) =>
      goto(MAgentDialing)
  }
  def canGoToCallOnPauseFromOnCall: StateFunction = {
    case Event(state: AgentOnPause, context: AgentStateContext) =>
      goto(MAgentOnCallOnPause)
  }
  def canGoToCallFromOnCallOnPause: StateFunction = {
    case Event(state: AgentReady, context: AgentStateContext) =>
      goto(MAgentOnCall)
  }
  def canGoToCallOnPause: StateFunction = {
    case Event(LineEvent(phoneNb,CallData(callId,LineState.UP,  _, variables )), context:AgentStateContext) if variables.get("XIVO_QUEUENAME").isEmpty=>
      goto(MAgentOnCallOnPause) using AgentStateContext(AgentOnCall(context.state,CallDirection.Incoming, acdState = false, paused=true), context.userStatus, context.monitorState, callId, phoneNumber= context.phoneNumber)
    case Event(LineEvent(phoneNb,CallData(callId,LineState.UP,  _, _ )), context:AgentStateContext) =>
      goto(MAgentOnCallOnPause) using AgentStateContext(AgentOnCall(context.state,CallDirection.Incoming, acdState = true, paused=true), context.userStatus, context.monitorState, callId, phoneNumber= context.phoneNumber)
  }
  def canGoToOutGoingCallOnPause: StateFunction = {
    case Event(LineEvent(phoneNb,CallData(callId,LineState.UP,  callerIdNum, _ )), context:AgentStateContext) if callerIdNum == context.phoneNumber =>
      goto(MAgentOnCallOnPause) using AgentStateContext(AgentOnCall(context.state,CallDirection.Outgoing, acdState = false, paused=true), context.userStatus, context.monitorState, callId, phoneNumber= context.phoneNumber)
  }
  def onFirstCallTerminatedGotoPause: StateFunction = {
    case Event(LineEvent(phoneNb, CallData(callId, LineState.HUNGUP, callerIdNum, _ )), context: AgentStateContext) if callId == context.callId && callerIdNum == context.phoneNumber  =>
      goto(MAgentPaused) using context.withState(AgentOnPause(context.state)).withCallId("")
  }
  def onFirstCallTerminatedGotoReady: StateFunction = {
    case Event(LineEvent(phoneNb, CallData(callId, LineState.HUNGUP, callerIdNum, _ )), context: AgentStateContext) if callId == context.callId && callerIdNum == context.phoneNumber =>
      goto(MAgentReady) using context.withState(AgentReady(context.state)).withCallId("")
  }

  def onCallTerminatedGotoReady: StateFunction = {
    case Event(LineEvent(phoneNb, CallData(callId, LineState.HUNGUP,  callerIdNum, _ )), context: AgentStateContext) if callerIdNum == context.phoneNumber =>
      goto(MAgentReady) using context.withState(AgentReady(context.state))
  }
  def onCallTerminatedGotoPause: StateFunction = {
    case Event(LineEvent(phoneNb, CallData(callId, LineState.HUNGUP,  callerIdNum, _ )), context: AgentStateContext) =>
      goto(MAgentPaused) using context.withState(AgentOnPause(context.state))
  }

  def onMasquerade: StateFunction = {
    case Event(LineEvent(phoneNb,CallData(callId,LineState.UP, callerIdNum, variables )), context:AgentStateContext) if callerIdNum == context.phoneNumber && variables.get("Masquerade").isDefined=>
      stay() using context.withCallId(callId)
  }

  def canGoToBasicStates: StateFunction = (canGoToLoggout orElse canGotoReady orElse canGoToPause)
}

class AgentStateFSM(val eventBus: XucEventBus) extends Actor
  with AgentStateCanGo
  with LoggingFSM[MAgentStates, MContext] {
  this: AgentStateTHandler =>

  import xivo.events.AgentState.{AgentLoggedOut, AgentLogin, AgentOnCall}

  log.info(s">>>>>>>>>>>Starting AgentStateFSM $self<<<<<<<<<<<<<<<")

  startWith(MInitState, MEmptyContext)

  when(MInitState)          (canGotoLogin orElse canGoToBasicStates)

  when(MAgentLogin)         (canGoToBasicStates)

  when(MAgentReady)         (canGoToLoggout orElse canGoToPause orElse canGoToWrapup orElse canGoToRinging orElse canGoToDialing orElse canGoToIncomingCall)

  when(MAgentPaused)        (canGoToLoggout orElse canGotoReady orElse canGoToWrapup  orElse canGoToRingingOnPause orElse canGoToDialingOnPause orElse canGoToCallOnPause)

  when(MAgentRinging)       (canGoToLoggout orElse canGoToIncomingCall orElse onCallTerminatedGotoReady orElse canGoToRingingOnPauseFromRinging)

  when(MAgentRingingOnPause)(canGoToLoggout orElse canGoToCallOnPause orElse onCallTerminatedGotoPause orElse canGoToRingingFromRingingOnPause)

  when(MAgentOnCall)        (canGoToLoggout orElse onMasquerade orElse canGoToWrapup orElse onFirstCallTerminatedGotoReady orElse canGoToCallOnPauseFromOnCall)

  when(MAgentOnCallOnPause) (canGoToLoggout orElse onMasquerade orElse onFirstCallTerminatedGotoPause orElse canGoToCallFromOnCallOnPause)

  when(MAgentDialing)       (canGoToLoggout orElse canGoToDialingOnPauseFromDialing orElse onCallTerminatedGotoReady orElse canGoToDirectOutgoingCall orElse canGoToOriginateOutboundCall)

  when(MAgentDialingOnPause) (canGoToLoggout orElse canGoToPause orElse canGoToDialing orElse canGoToDialingFromDialingOnPause orElse canGoToOutGoingCallOnPause)

  when(MAgentOnWrapup)      (canGoToBasicStates orElse canGoToDialing orElse canGoToRinging)

  when(MAgentLoggedOut)     (canGotoLogin)

  onTransition(lineEventSubs.subscribe)
  onTransition(eventPublisher.publish)
  onTransition(transitionPublisher.publish)

  whenUnhandled {

    case Event(agentUserStatus: AgentUserStatus, lastState: AgentStateContext) =>
//      stay()
      val updatedState = lastState.state withCause agentUserStatus.userStatus withPhoneNb lastState.phoneNumber
      log.debug(s"whenUnhandled $lastState agentUserStatus publishing $updatedState with $agentUserStatus")
      eventBus.publish(updatedState)
      stay using AgentStateContext(updatedState, agentUserStatus.userStatus, lastState.monitorState, lastState.callId, lastState.phoneNumber)

    case Event(glrs: GetLastReceivedState, lastState: AgentStateContext) =>
      log.debug(s"last state requested ${lastState.state} for ${glrs.requester}")
      lastState.state match {
        case ls: AgentLoggedOut =>
          glrs.requester ! ls
        case ls =>
          glrs.requester ! AgentLogin(lastState.state)
          glrs.requester ! ls
      }
      stay()

    case Event(acu: AgentCallUpdate, context: AgentStateContext) =>
      log.debug(s"Received agentCallUpdate: $acu in state ${context.state}")
      context.state match {
        case aoc: AgentOnCall if(acu.monitorState != context.monitorState) =>
          log.debug(s"whenUnhandled   AgentOnCall AgentCallUpdate publishing $aoc with $acu")
          eventBus.publish(aoc withCause context.userStatus withMonitorState acu.monitorState withPhoneNb context.phoneNumber)
          stay using (context withMonitorState acu.monitorState withState(aoc withCause context.userStatus withMonitorState(acu.monitorState)))
        case any =>
          stay using context.withMonitorState(acu.monitorState)
      }

    case Event(state:AgentState, context: AgentStateContext) if state.queues != context.state.queues =>
      log.debug(s"whenUnhandled  queues updated publishing state $state with updated queues from $context")
      publishSaveContextAndStay(state,context)

    case unknown => log.debug(s">>>>unhandled $unknown on state ${stateName}")
      stay()
  }

  def publishSaveContextAndStay(state:AgentState, context: AgentStateContext) = {
    eventBus.publish(state withPhoneNb context.phoneNumber)
    stay using AgentStateContext(state, context.userStatus, context.monitorState, context.callId, context.phoneNumber)
  }

  initialize()

}
