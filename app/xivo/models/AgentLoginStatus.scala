package xivo.models

import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime
import play.api.db.DB
import play.api.Play.current

case class AgentLoginStatus(id: Agent.Id, phoneNumber:String, loginDate: DateTime)

trait AgentLoginStatusDao {
  def getLoginStatus(id:Agent.Id):Option[AgentLoginStatus]

}

trait AgentLoginStatusDaoDb extends AgentLoginStatusDao {
  def getLoginStatus(id:Agent.Id):Option[AgentLoginStatus] = AgentLoginStatus.getLoginStatus(id)
}

object AgentLoginStatus {

  val simple = {
    get[Long]("agent_id") ~
      get[String]("extension") ~
      get[Date]("login_at") map {
      case id ~ phoneNumber ~ loginDate =>
        AgentLoginStatus(id, phoneNumber, new DateTime(loginDate))
    }
  }

  def getLoginStatus(id:Agent.Id):Option[AgentLoginStatus] =
    DB.withConnection {
      implicit c =>
        SQL("select agent_id, extension, login_at from agent_login_status where agent_id = {id}").on('id -> id).as(simple.singleOpt)
    }

}