package xivo.models

import scala.concurrent.Await
import play.api.libs.json._
import scala.concurrent.duration.DurationInt
import xivo.network.XiVOWS
import play.api.http.Status._
import play.api.Logger

import scala.language.postfixOps

case class XivoDevice(id: String, ip: Option[String], vendor: String)
case class XivoLine(id: Int, context: String, protocol:String, name: String, device_id: Option[String])
case class Line(id: Int, context: String, protocol:String, name: String, dev: Option[XivoDevice]) {
  def interface =  s"${protocol.toUpperCase}/$name"
}

trait LineFactory {
  def get(Id: Line.Id): Option[Line]
}

object Line extends LineFactory {
  type Id = Long
  val waitResult = 10 seconds

  val logger = Logger(getClass.getName)
  implicit val context = scala.concurrent.ExecutionContext.Implicits.global
  implicit val lineReads = Json.reads[XivoLine]
  implicit val deviReads = Json.reads[XivoDevice]

  def get(Id: Line.Id): Option[Line] = {
    getXiVOLine(Id) match {
      case Some(xivoline) =>
        Some(Line(xivoline.id, xivoline.context, xivoline.protocol, xivoline.name, getDevice(xivoline.device_id.getOrElse("unknown"))))
      case _ => None
    }
  }

  private def getXiVOLine(id: Line.Id): Option[XivoLine] = {
    val responseFuture = XiVOWS.withWS(s"lines/$id").get()
    val resultFuture = responseFuture map { response =>
      response.status match {
        case OK =>
          val xivoLine = (response.json).validate[XivoLine].get
          logger.debug("Returning xivoLine: " + xivoLine)
          Some(xivoLine)
        case default => {
          logger.debug(s"Webservice get line for id: $id failed.")
          None
        }
      }
    }
    Await.result(resultFuture, waitResult)
  }

  def all: List[Line] = {
    for (xivoLine <- allLines) yield Line(xivoLine.id, xivoLine.context, xivoLine.protocol, xivoLine.name, getDevice(xivoLine.device_id.getOrElse("unknown")))
  }
  private def allLines: List[XivoLine] = {
    val responseFuture = XiVOWS.withWS("lines").get()
    val resultFuture = responseFuture map {
      response =>
        (response.json \ "items").validate[List[XivoLine]].get
    }
    Await.result(resultFuture, waitResult)
  }

  def getDevice(id: String): Option[XivoDevice] = {
    val responseFuture = XiVOWS.withWS(s"devices/$id").get()
    val resultFuture = responseFuture map { response =>
      if (response.status == OK)
        Some((response.json).validate[XivoDevice].get)
      else
        None
    }
    Await.result(resultFuture, waitResult)
  }
}