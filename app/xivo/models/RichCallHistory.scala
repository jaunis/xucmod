package xivo.models

import org.joda.time.format.{PeriodFormatterBuilder, DateTimeFormat}
import org.joda.time.{DateTime, Period}
import play.api.libs.json.{Json, JsArray, Writes}
import services.config.ConfigRepository

case class RichCallDetail(start: DateTime,
                          duration: Option[Period],
                          srcUsername: String,
                          dstUsername: String,
                          status: CallStatus.CallStatus)
object RichCallDetail {
  val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  val periodFormat = new PeriodFormatterBuilder()
  .minimumPrintedDigits(2).printZeroAlways().appendHours().appendSeparator(":").appendMinutes().appendSeparator(":").appendSeconds().toFormatter

  def apply(cd: CallDetail): RichCallDetail = RichCallDetail(cd.start,
  cd.duration,
  ConfigRepository.repo.userNameFromPhoneNb(cd.srcNum).getOrElse(""),
  ConfigRepository.repo.userNameFromPhoneNb(cd.dstNum).getOrElse(""),
  cd.status)

  implicit val writes = new Writes[RichCallDetail] {
    def writes(call: RichCallDetail) = Json.obj(
    "start" -> call.start.toString(format),
    "duration" -> call.duration.map(_.toString(periodFormat)),
    "srcUsername" -> call.srcUsername,
    "dstUsername" -> call.dstUsername,
    "status" -> call.status.toString
    )
  }
}
case class RichCallHistory(calls: List[RichCallDetail])

object RichCallHistory {
  def apply(history: CallHistory): RichCallHistory = RichCallHistory(history.calls.map(RichCallDetail.apply))

  implicit val writes = new Writes[RichCallHistory] {
    override def writes(h: RichCallHistory) = JsArray(h.calls.map(Json.toJson(_)))
  }
}