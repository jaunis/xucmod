package xivo.phonedevices

import akka.actor.ActorRef
import org.xivo.cti.MessageFactory
import org.xivo.cti.model.Endpoint
import xivo.models.Line
import xivo.xuc.XucConfig

class CtiDevice(val line:Line) extends DeviceAdapter {
  val messageFactory = new MessageFactory

  override def dial(destination: String, sender: ActorRef) = sender ! StandardDial(line, destination, XucConfig.xivoHost)
  override def answer(sender: ActorRef) = {}
  override def hangup(sender: ActorRef) = sender ! messageFactory.createHangup
  override def attendedTransfer(destination: String, sender: ActorRef) = sender ! messageFactory.createAttendedTransfer(destination)
  override def completeTransfer(sender: ActorRef) = sender ! messageFactory.createCompleteTransfer
  override def cancelTransfer(sender: ActorRef) =  sender ! messageFactory.createCancelTransfer
  override def conference(sender: ActorRef) = {}
  override def hold(sender: ActorRef) = {}

}