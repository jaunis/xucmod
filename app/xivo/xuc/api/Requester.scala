package xivo.xuc.api

import services.agent.{LoginProcessor, LogoutAgent, LogoutProcessor}
import services.config.ConfigRepository
import services.request.{AgentLoginRequest, AgentTogglePause, BaseRequest}
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import akka.pattern.ask
import akka.util.Timeout
import akka.util.Timeout.durationToTimeout
import play.api.Logger
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.concurrent.Akka.system
import services._
import xivo.network.LoggedOn
import xivo.xuc.XucConfig
import models.XucUser



class RequestResult()
case class RequestTimeout(reason: String) extends RequestResult
case class RequestSuccess(reason: String) extends RequestResult
case class RequestError(reason: String) extends RequestResult

object Requester {
  import services.config.ConfigManager._

  val log = Logger(getClass.getName)
  val ctiRouterFactory = CtiRouterFactory.factory
  implicit val timeout: akka.util.Timeout = 5 seconds
  val connectTimeout = 10 seconds

  def handShake() = {
    log.info(s"Executing handshake")

    Akka.system.actorSelection(s"/user/MainRunner/${ActorFactory.ConfigManagerId}") ! PublishUserPhoneStatuses
  }

  def togglePause(phoneNumber: String) = {
    val req = BaseRequest(Akka.system.deadLetters, AgentTogglePause(phoneNumber))

    Akka.system.actorSelection(s"/user/MainRunner/${ActorFactory.ConfigManagerId}") ! req
  }

  def agentLogin(agentLoginReq: AgentLoginRequest): Future[RequestResult] = {
    val processor = system.actorOf(LoginProcessor.props())
    implicit val timeout = Timeout(3 seconds)
    val result = processor ? agentLoginReq
    result.mapTo[RequestResult]
  }

  def agentLogout(phoneNb: String): Future[RequestResult] = {
    val processor = system.actorOf(LogoutProcessor.props())
    implicit val timeout = Timeout(3 seconds)
    val result = processor ? LogoutAgent(phoneNb)
    result.mapTo[RequestResult]
  }

  def execute(username: String, ctiRequest: org.json.JSONObject): Future[RequestResult] = {
    log.debug(s"Executing request for $username")

    val resp: Future[Any] = (destination(username) ? ctiRequest)
    val timeoutFuture = play.api.libs.concurrent.Promise.timeout(RequestTimeout("timed out"), 2 seconds)

    Future.firstCompletedOf(Seq(resp, timeoutFuture)).flatMap {
      respResult =>
        respResult match {
          case t: RequestTimeout =>
            log.debug(s"timeout on executing request for $username actor not started")
            connectAndExecute(username, ctiRequest)
          case any =>
            log.debug(s"execution result: ${any.toString}")
            Future(RequestSuccess("Request processed"))
        }
    }
  }

  private def connectAndExecute(username: String, ctiRequest: org.json.JSONObject): Future[RequestResult] = {
    log.debug(s"connectAndExecute $username")
    connect(username, XucConfig.DefaultPassword)

    log.debug(s"process request $username")
    val resp: Future[Any] = (destination(username) ? ctiRequest)
    val timeoutFuture = play.api.libs.concurrent.Promise.timeout(RequestTimeout("timed out"), connectTimeout)

    Future.firstCompletedOf(Seq(resp, timeoutFuture)).map {
      case CtiReqSuccess() =>
        log.debug("connectAndExecute Request success")
        RequestSuccess("Request processed")
      case t: RequestTimeout =>
        log.debug("connectAndExecute Request timeout")
        t
      case LoggedOn =>
        log.debug("logged on received")
        RequestSuccess("Request processed")
      case _ =>
        log.debug("connectAndExecute **** Request error")
        RequestError("Unexpected")
    }
  }
  private def destination(username: String): akka.pattern.AskableActorSelection = {
    Akka.system.actorSelection(s"/user/${username}CtiRouter")
  }

  def connect(username: String, password: String): Future[RequestResult] = {
    log.debug(s"connect $username")

    val user = ConfigRepository.repo.getUser(username) match {
      case Some(xivoUser) => XucUser(xivoUser.username, xivoUser.password.getOrElse(""))
      case None => XucUser(username,XucConfig.DefaultPassword)
    }
    log.debug(s"got user $user from config repo")
    val ctiRouter = ctiRouterFactory.getRouter(user)

    val timeoutFuture = play.api.libs.concurrent.Promise.timeout(RequestTimeout("connect timeout"), connectTimeout)
    val startResult = ctiRouter ? Start(user)

    Future.firstCompletedOf(Seq(startResult, timeoutFuture)).map {
      case st: Started => {
        log.debug("Login of user: \"" + username + "\" to the CTI server Ok")
        RequestSuccess("Request processed")
      }
      case t: RequestTimeout =>
        log.error(s"connect $username Request timeout")
        t
      case x =>
        log.error(s"unable to connect $username $x")
        RequestError(s"unable to connect $username $x")
    }
  }

}
