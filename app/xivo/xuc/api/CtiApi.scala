package xivo.xuc.api

import scala.concurrent.Future
import org.xivo.cti.MessageFactory
import play.api.Logger
import org.xivo.cti.model.Endpoint.EndpointType
import org.xivo.cti.model.Endpoint

object CtiApi {
  val log = Logger(getClass.getName)
  val messageFactory = new MessageFactory

  def dnd(username: String, state: Boolean): Future[RequestResult] =
    Requester.execute(username, messageFactory.createDND(state))

  def dial(username: String, number: String): Future[RequestResult] =
    Requester.execute(username, messageFactory.createDial(new Endpoint(EndpointType.EXTEN, number)))

  def uncForward(username: String, state: Boolean, destination: String): Future[RequestResult] =
    Requester.execute(username, messageFactory.createUnconditionnalForward(destination, state))

  def naForward(username: String, state: Boolean, destination: String): Future[RequestResult] =
    Requester.execute(username, messageFactory.createNaForward(destination, state))

  def busyForward(username: String, state: Boolean, destination: String): Future[RequestResult] =
    Requester.execute(username, messageFactory.createBusyForward(destination, state))

}