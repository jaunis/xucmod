package xivo.xuc

import play.api.Play.current
import scala.collection.JavaConverters._

object XucConfig {
  def xivoHost: String = current.configuration.getString("xivohost").getOrElse("")

  def outboundLength:Int = current.configuration.getInt("xuc.outboundLength").getOrElse(6)

  val XivoCtiIdentitySuffix = "XuC"
  def EventUser: String = current.configuration.getString("xivocti.eventUser").getOrElse("")
  def XivoCtiTimeout = current.configuration.getString("xivocti.timeout").getOrElse("1").toInt

  def XivoWs_host = current.configuration.getString("XivoWs.host").getOrElse("")
  def XivoWs_port = current.configuration.getString("XivoWs.port").getOrElse("")
  def XivoWs_wsUser = current.configuration.getString("XivoWs.wsUser").getOrElse("")
  def XivoWs_wsPwd = current.configuration.getString("XivoWs.wsPwd").getOrElse("")

  def EVENT_URL = current.configuration.getString("api.eventUrl").getOrElse("")
  def DefaultPassword = current.configuration.getString("xivocti.defaultPassword").getOrElse("")
  
  val XIVOCTI_HOST:String = current.configuration.getString("xivocti.host").getOrElse("localhost")
  val XIVOCTI_PORT:String = current.configuration.getString("xivocti.port").getOrElse("5003")


  def SnomUser = current.configuration.getString("Snom.user").getOrElse("")
  def SnomPassword = current.configuration.getString("Snom.password").getOrElse("")

  def metricsRegistryName: String = current.configuration.getString("techMetrics.name").getOrElse("techMetrics")
  def metricsRegistryJVM: Boolean = current.configuration.getBoolean("techMetrics.jvm").getOrElse(true)
  def metricsLogReporter: Boolean = current.configuration.getBoolean("techMetrics.logReporter").getOrElse(true)
  def metricsLogReporterPeriod: Long = current.configuration.getLong("techMetrics.logReporterPeriod").getOrElse(5)

  def xucPeers : List[String] = current.configuration.getStringList("xuc.peers").getOrElse(new java.util.ArrayList[String]()).asScala.toList

  def recordingHost: String = current.configuration.getString("recording.host").getOrElse("localhost")
  def recordingPort: Int = current.configuration.getInt("recording.port").getOrElse(9000)
  def recordingLogin: String = current.configuration.getString("recording.login").getOrElse(EventUser)
  def recordingPassword: String = current.configuration.getString("recording.password").getOrElse(DefaultPassword)

  def rightsHost: String = current.configuration.getString("rights.host").getOrElse("")
  def rightsPort: Int = current.configuration.getInt("rights.port").getOrElse(0)
}