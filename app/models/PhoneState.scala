package models

import org.xivo.cti.model.PhoneHintStatus
import org.xivo.cti.message.PhoneStatusUpdate

case class PhoneState(state: PhoneHintStatus)

object PhoneState {
  def getPhoneState(phoneStatusUpdate: PhoneStatusUpdate): PhoneState = {
    new PhoneState(PhoneHintStatus.getHintStatus(Integer.parseInt(phoneStatusUpdate.getHintStatus)) )
  }
}