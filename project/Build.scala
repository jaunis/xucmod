import sbt._
import sbt.Keys._
import play.Play.autoImport._

object ApplicationBuild extends Build {

  val appName = "xucmod"
  val appVersion = "2.35"
  val appOrganisation = "xivo"

  lazy val xucstats = ProjectRef(file("../xucstats"),"xucstats")
  lazy val xucami = ProjectRef(file("../xucami"),"xucami")

  val main = Project(appName, file("."))
    .enablePlugins(play.PlayScala)
    .settings(
      version := appVersion,
      scalaVersion := Dependencies.scalaVersion,
      organization := appOrganisation,
      resolvers     ++= Dependencies.resolutionRepos,
      libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
      publishArtifact in (Compile, packageDoc) := false,
      publishArtifact in packageDoc := false
    )
    .settings(
      testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u","target/test-reports","-l","xuc.tags.IntegrationTest"),
      testOptions in integrationTest := Seq(Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u","target/integrationtest-reports","-n","xuc.tags.IntegrationTest")),
      testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u", "target/test-reports")
    )
    .configs(integrationTest)
    .settings(inConfig(integrationTest)(Defaults.testTasks): _*)
    //.dependsOn(xucami)
    //.aggregate(xucami)

  lazy val integrationTest = config("integrationTest") extend(Test)
}
